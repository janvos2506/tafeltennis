DROP TRIGGER trg_areTeamsAvailable  
GO

CREATE TRIGGER trg_areTeamsAvailable ON Wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 select * from wedstrijd
			 where TEAMNR1 is null OR TEAMNR2 is null
			 AND WEDSTRIJDNR in (select WEDSTRIJDnr from inserted)
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Er zijn te weinig teams aan deze wedstrijd gekoppeld';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE proc_createWedstrijd
GO
CREATE PROCEDURE proc_createWedstrijd
@team1 INT,
@team2 INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	DECLARE @indelingnr INT
	DECLARE @poulenr INT
	DECLARE @tafelnr INT
	DECLARE @toernooirnr INT

	SET @indelingnr = (select indelingnr from team where teamnr = @team1)
	SET @poulenr = (select poulenr from team where teamnr = @team1)
	SET @toernooirnr = (select toernooinr from klasseindeling where indelingnr = @indelingnr)
	SET @tafelnr = (select tafelnr from tafelintoernooi where toernooinr = @toernooirnr)

		  insert into wedstrijd (teamnr1, teamnr2, indelingnr, poulenr, tafelnr, winnaar, startdatumtijd,einddatumtijd)
		  values (@team1,@team2,@indelingnr,@poulenr,@tafelnr,null,null,null)
		  PRINT 'Wedstrijd aangemaakt'
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP PROC proc_createWedstrijdLaddertoernooi
GO
CREATE PROCEDURE proc_createWedstrijdLaddertoernooi
@team1 INT,
@team2 INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
		      ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	DECLARE @indelingnr INT
	DECLARE @poulenr INT
	DECLARE @tafelnr INT
	DECLARE @toernooirnr INT
	DECLARE @lastTimePlayed DATETIME

	SET @indelingnr = (select indelingnr from team where teamnr = @team1)
	SET @poulenr = (select poulenr from team where teamnr = @team1)
	SET @toernooirnr = (select toernooinr from klasseindeling where indelingnr = @indelingnr)
	SET @tafelnr = (select tafelnr from tafelintoernooi where toernooinr = @toernooirnr)
	SET @lastTimePlayed = ( SELECT TOP 1 EindDatumTijd from wedstrijd W
							WHERE (Teamnr1 = @team1 AND teamnr2 = @team2)
							   OR (Teamnr1 = @team2 AND teamnr1 = @team2)
							ORDER BY EindDatumTijd DESC)
							
	IF (@lastTimePlayed > dateadd(MONTH, -2, getDATE()))
			
	BEGIN
					 DECLARE @errormsg VARCHAR(MAX)= 'Deze twee teams mogen nog geen wedstrijd spelen. Er moet minimaal twee maanden wachttijd tussen zitten.';
                     THROW 50004, @errormsg, 1;
	END

	DECLARE @team1Score INT
	DECLARE @team2Score INT

	SET @team1Score = (select score from team where teamnr = @team1)
	SET @team2Score = (select score from team where teamnr = @team2)

	IF (@team1Score < (@team2score -3)) OR (@team1Score > (@team2score + 3))

	BEGIN
					 DECLARE @errormsg2 VARCHAR(MAX)= 'Deze twee teams mogen geen wedstrijd spelen. Hun rang ligt te ver uit elkaar.';
                     THROW 50004, @errormsg2, 1;
	END

		  insert into wedstrijd (teamnr1, teamnr2, indelingnr, poulenr, tafelnr, winnaar, startdatumtijd,einddatumtijd)
		  values (@team1,@team2,@indelingnr,@poulenr,@tafelnr,null,getdate(),null)
		  PRINT 'Wedstrijd aangemaakt'

        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                 ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP TRIGGER trg_updateWachttijd
GO

CREATE TRIGGER trg_updateWachttijd ON Wedstrijd
AFTER UPDATE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF UPDATE (WINNAAR)
			 BEGIN
			 
			 UPDATE TEAM
			 SET wachttijd = getdate()
			 WHERE TEAMNR = (select TEAMNR1 from inserted)

			 UPDATE TEAM
			 SET wachttijd = getdate()
			 WHERE TEAMNR = (select TEAMNR2 from inserted)

			 END
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE proc_startWedstrijd
GO
CREATE PROCEDURE proc_startWedstrijd
@wedstrijdnr INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

		DECLARE @poulenr INT
		DECLARE @indelingnr INT
		DECLARE @startdatum DATETIME
		DECLARE @einddatum DATETIME

		SET @poulenr = (SELECT POULENR FROM WEDSTRIJD WHERE WEDSTRIJDNR = @wedstrijdnr)
		SET @indelingnr = (SELECT INDELINGNR FROM WEDSTRIJD WHERE WEDSTRIJDNR = @wedstrijdnr)
		SET @startdatum =
		(
				select BEGINDATUMTIJD
				from TOERNOOI T INNER JOIN KLASSEINDELING K ON T.TOERNOOINR = K.TOERNOOINR
				INNER JOIN POULE P ON P.INDELINGNR = K.INDELINGNR
				WHERE P.POULENR = @poulenr AND P.INDELINGNR = @indelingnr
		)

		SET @einddatum =
		(
				select EINDDATUMTIJD
				from TOERNOOI T INNER JOIN KLASSEINDELING K ON T.TOERNOOINR = K.TOERNOOINR
				INNER JOIN POULE P ON P.INDELINGNR = K.INDELINGNR
				WHERE P.POULENR = @poulenr AND P.INDELINGNR = @indelingnr
		)

		IF (@startdatum > getdate())
		BEGIN
					 DECLARE @errormsg VARCHAR(MAX)= 'Dit toernooi is nog niet begonnen';
                     THROW 50004, @errormsg, 1;
		END

		IF (@einddatum < getdate())
		BEGIN
					 DECLARE @errormsg2 VARCHAR(MAX)= 'Dit toernooi is al afgelopen.';
                     THROW 50004, @errormsg2, 1;
		END

		UPDATE WEDSTRIJD
		SET STARTDATUMTIJD = getdate()
		WHERE WEDSTRIJDNR = @wedstrijdnr

		PRINT 'Wedstrijd is begonnen'
	
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

-- Constraint
DROP TRIGGER trg_isWedstrijdNogBezig
GO

CREATE TRIGGER trg_isWedstrijdNogBezig ON Wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT 1 from WEDSTRIJD W
			 WHERE EINDDATUMTIJD is null AND WEDSTRIJDNR NOT IN (select WEDSTRIJDNR from inserted)

			 AND(    W.TEAMNR1 IN (select TEAMNR1 from inserted)
				 OR W.TEAMNR1 IN (select TEAMNR2 from inserted)
				 OR W.TEAMNR2 IN (select TEAMNR1 from inserted)
				 OR W.TEAMNR2 IN (select TEAMNR2 from inserted)
				 )
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= '1 van de teams moet nog een wedstrijd spelen';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER trg_isTableAvailable  
GO

CREATE TRIGGER trg_isTableAvailable ON Wedstrijd
AFTER UPDATE, INSERT 
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT * FROM WEDSTRIJD
			 WHERE EINDDATUMTIJD is null
				AND TAFELNR IN (select tafelnr from inserted)
				AND WEDSTRIJDNR NOT IN (select WEDSTRIJDNR from inserted)
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze tafel is nog niet beschikbaar';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER trg_TableAvailable
GO

CREATE TRIGGER trg_TableAvailable ON wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
		 IF((select tafelnr from inserted) is not null)
		 BEGIN
             IF NOT EXISTS
             (
			 select * from TAFELINTOERNOOI where TAFELNR in ( select tafelnr from inserted)
			 
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze tafel is niet beschikbaar voor dit toernooi';
                     THROW 50004, @errormsg, 1;
                 END;
				 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER trg_isTableKnown
GO

CREATE TRIGGER trg_isTableKnown ON Wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
		 IF UPDATE(STARTDATUMTIJD)
		 BEGIN
             IF EXISTS
             (
			 SELECT * FROM WEDSTRIJD
			 WHERE TAFELNR IS NULL
			 AND WEDSTRIJDNR IN (SELECT WEDSTRIJDNR FROM inserted)
			 AND STARTDATUMTIJD IS NOT NULL
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze wedstrijd kan nog niet beginnen, de tafel is nog niet bekend';
                     THROW 50004, @errormsg, 1;
                 END;
		 END
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER trg_checkOpDubbelTeams
GO

CREATE TRIGGER trg_checkOpDubbelTeams ON Team
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT T.TEAMNR
			 FROM KLASSEINDELING K 
			 INNER JOIN TEAM T 
				ON T.INDELINGNR = K.INDELINGNR
			 WHERE (T.LID2_LIDNR is null OR T.LID2_VERENIGINGCODE is null) AND K.DUBBEL = 1
             ) 
		   OR EXISTS (
			 SELECT T.TEAMNR
			 FROM KLASSEINDELING K 
			 INNER JOIN TEAM T 
				ON T.INDELINGNR = K.INDELINGNR
			 WHERE (T.LID2_LIDNR is not null OR T.LID2_VERENIGINGCODE is not null) AND K.DUBBEL = 0
			 )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Verkeerde hoeveelheid spelers voor dit toernooi';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE proc_checkOnValidGame
GO

CREATE PROCEDURE proc_checkOnValidGame
@teamnr1 INT,
@teamnr2 INT,
@toernooinr INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY

	IF EXISTS
	(
	select 1 from wedstrijd W
				where W.INDELINGNR IN (
						select P.INDELINGNR
						from Toernooi T INNER JOIN KLASSEINDELING K ON T.TOERNOOINR = K.TOERNOOINR
						INNER JOIN POULE P ON P.INDELINGNR = K.INDELINGNR
						WHERE T.TOERNOOINR = @toernooinr)
				 AND W.POULENR IN (
				 select P.POULENR
						from Toernooi T INNER JOIN KLASSEINDELING K ON T.TOERNOOINR = K.TOERNOOINR
						INNER JOIN POULE P ON P.INDELINGNR = K.INDELINGNR
						WHERE T.TOERNOOINR = @toernooinr)
				AND W.EINDDATUMTIJD > (DATEADD(MONTH,-2,GETDATE()))
				AND W.TEAMNR1 = @teamnr1
				AND W.TEAMNR2 = @teamnr2
	)
	BEGIN
	RAISERROR('Deze wedstrijd mag niet gespeeld worden, er zit niet genoeg tijd tussen.',1,1);
	END
	ELSE
	PRINT 'Deze wedstrijd mag gespeeld worden.'
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO


DROP PROCEDURE sp_maakLaddertoernooiAan
GO

CREATE PROCEDURE sp_maakLaddertoernooiAan @toernooinr INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
     BEGIN TRY
		  -- Code here

		  UPDATE
			 TEAM
		  SET
			 TEAM.SCORE = TEAM_B.SCORE
		  FROM
			 TEAM AS Table_A
			 INNER JOIN (    
				SELECT 1000 - ROW_NUMBER() OVER(ORDER BY l.KLASSECODE DESC) AS SCORE, t.TEAMNR, l.KLASSECODE FROM TEAM t
				    INNER JOIN POULE p
					   ON p.POULENR = t.POULENR
				    INNER JOIN LID l
					   ON l.LIDNR = t.LID1_LIDNR
				    INNER JOIN KLASSEINDELING i
					   ON i.INDELINGNR = p.INDELINGNR
				    INNER JOIN TOERNOOI te
					   ON te.TOERNOOINR = i.TOERNOOINR
				WHERE te.TOERNOOINR = @toernooinr) AS TEAM_B
		  ON Table_A.TEAMNR = TEAM_B.TEAMNR

		  --End Code
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP TRIGGER tr_checkEisenDeelnameToernooi;
GO

CREATE TRIGGER tr_checkEisenDeelnameToernooi ON TEAM
AFTER INSERT, UPDATE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS (
				SELECT * FROM TOERNOOI t INNER JOIN KLASSEINDELING k 
							ON t.TOERNOOINR = k.TOERNOOINR
						 INNER JOIN POULE p 
							ON k.INDELINGNR = p.INDELINGNR
						 INNER JOIN INSERTED i
						    ON i.POULENR = p.POULENR
						 WHERE t.STARTINSCHRIJF > GETDATE()
							OR t.EINDINSCHRIJF < GETDATE()
			 )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Inschrijven kan niet buiten de inschrijftermijn';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER tr_checkEisenStartToernooiMax;
GO

CREATE TRIGGER tr_checkEisenStartToernooiMax ON TEAM
AFTER INSERT, UPDATE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS (	/* Maximaal aantal teams in een toernooi kan niet worden overschreden */
			 		SELECT t.TOERNOOINR FROM TOERNOOI t INNER JOIN KLASSEINDELING k 
							ON t.TOERNOOINR = k.TOERNOOINR
						 INNER JOIN POULE p 
							ON k.INDELINGNR = p.INDELINGNR
						 INNER JOIN TEAM te
						    ON te.POULENR = p.POULENR
						 GROUP BY t.TOERNOOINR, t.MAXDEELNEMERS
						 HAVING COUNT(te.TEAMNR) > t.MAXDEELNEMERS
			 )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Maximaal aantal teams in een toernooi kan niet worden overschreden';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP TRIGGER tr_checkEisenStartToernooiMin;
GO

CREATE TRIGGER tr_checkEisenStartToernooiMin ON TEAM
AFTER DELETE, UPDATE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS (	/* Minimum aantal deelnemers wordt niet meer gehaald */
				SELECT t.TOERNOOINR, t.MINDEELNEMERS, p.POULENR FROM TOERNOOI t
					INNER JOIN KLASSEINDELING k 
						ON t.TOERNOOINR = k.TOERNOOINR
					INNER JOIN POULE p 
						ON k.INDELINGNR = p.INDELINGNR
					GROUP BY t.TOERNOOINR, t.MINDEELNEMERS, p.POULENR
					HAVING (SELECT COUNT(te.TEAMNR) FROM TEAM te WHERE te.POULENR = p.POULENR) < T.MINDEELNEMERS
			 )
				BEGIN
					PRINT 'Minimum aantal deelnemers wordt nog niet gehaald';
                END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROC sp_afsluitenWedstrijd
GO

CREATE PROCEDURE sp_afsluitenWedstrijd(
       @wedstrijdnr INT)
AS
     BEGIN
         SET NOCOUNT , XACT_ABORT ON;
         DECLARE @TranCounter INT;
         SET @TranCounter = @@TRANCOUNT;
         IF @TranCounter > 0
             BEGIN
                 SAVE TRANSACTION ProcedureSave;
             END;
         ELSE
             BEGIN
                 BEGIN TRANSACTION;
             END;
         BEGIN TRY
             --Check wedstrijd al ingepland.
             -- Check of de wedstrijd al is afgesloten
             IF EXISTS ( SELECT 1
                         FROM WEDSTRIJD
                         WHERE WEDSTRIJDNR = @wedstrijdnr
                               AND
                               EINDDATUMTIJD IS NOT NULL
                               OR
                               WINNAAR IS NOT NULL
                               AND
                               WEDSTRIJDNR = @wedstrijdnr
                               OR
                               WEDSTRIJDNR = @wedstrijdnr
                               AND
                               STARTDATUMTIJD IS NULL
                       )
                OR
                NOT EXISTS ( SELECT 1
                             FROM WEDSTRIJD
                             WHERE WEDSTRIJDNR = @wedstrijdnr
                           )
                 BEGIN
                     RAISERROR('De wedstrijd kan niet worden afgesloten of bestaat niet!' , 16 , 1);
                 END;
                     --Wedstrijd afsluiten en ranglijst updaten voor laddertoernooi.
             ELSE
                 BEGIN
                     DECLARE @winnaarRatio INT;
                     SELECT @winnaarRatio = ( ( SELECT COUNT(SETNR)
                                                FROM SETSCORE AS S
                                                WHERE TEAM1SCORE > TEAM2SCORE
                                                      AND
                                                      S.WEDSTRIJDNR = @wedstrijdnr
                                              ) - ( SELECT COUNT(SETNR)
                                                    FROM SETSCORE AS S
                                                    WHERE TEAM2SCORE > TEAM1SCORE
                                                          AND
                                                          S.WEDSTRIJDNR = @wedstrijdnr
                                                  ) );
                     DECLARE @winnaar INT;
                     DECLARE @winnaarScore INT;
                     DECLARE @verliezer INT;
                     DECLARE @verliezerScore INT;
                     SELECT @winnaar = ( SELECT ( CASE
                                                      WHEN @winnaarRatio < 0
                                                      THEN TEAMNR2
                                                      WHEN @winnaarRatio > 0
                                                      THEN TEAMNR1
                                                  END ) AS WINNAAR
                                         FROM WEDSTRIJD
                                         WHERE WEDSTRIJDNR = @wedstrijdnr
                                       );
                     SELECT @winnaarScore = ( SELECT SCORE
                                              FROM TEAM
                                              WHERE TEAMNR = @winnaar
                                            );
                     SELECT @verliezer = ( SELECT ( CASE
                                                        WHEN @winnaarRatio > 0
                                                        THEN TEAMNR2
                                                        WHEN @winnaarRatio < 0
                                                        THEN TEAMNR1
                                                    END ) AS VERLIEZER
                                           FROM WEDSTRIJD
                                           WHERE WEDSTRIJDNR = @wedstrijdnr
                                         );
                     SELECT @verliezerScore = ( SELECT SCORE
                                                FROM TEAM
                                                WHERE TEAMNR = @verliezer
                                              );
                     UPDATE WEDSTRIJD
                            SET EINDDATUMTIJD = GETDATE() , WINNAAR = @winnaar
                     WHERE WEDSTRIJDNR = @wedstrijdnr;

					    /*
					    - Team wint maar staat hoger dan de verliezer
					    - Team wint en gaat een plek omhoog
					    - Team verliest maar is al lager dan de winnaar
					    - Team verliest en gaat een plek naar beneden
					    */

                     UPDATE TEAM
                            SET SCORE = ( CASE
                                              WHEN @winnaar = TEAMNR
                                                   AND
                                                   SCORE < @verliezerScore
                                              THEN @verliezerScore
                                              WHEN @verliezer = TEAMNR
                                                   AND
                                                   SCORE > @winnaarScore
                                              THEN @winnaarScore
                                              ELSE SCORE
                                          END )
                     WHERE TEAMNR = ( SELECT TEAMNR1
                                      FROM WEDSTRIJD
                                      WHERE WEDSTRIJDNR = @wedstrijdnr
                                    )
                           OR
                           TEAMNR = ( SELECT TEAMNR2
                                      FROM WEDSTRIJD
                                      WHERE WEDSTRIJDNR = @wedstrijdnr
                                    );
                 END;
             IF @TranCounter = 0
                AND
                XACT_STATE() = 1
                 BEGIN
                     COMMIT TRANSACTION;
                 END;
         END TRY
         BEGIN CATCH
             DECLARE @ErrorMessage VARCHAR(400) , @ErrorSeverity INT , @ErrorState INT;
             SELECT @ErrorMessage = ERROR_MESSAGE() , @ErrorSeverity = ERROR_SEVERITY() , @ErrorState = ERROR_STATE();
             RAISERROR(@ErrorMessage , @ErrorSeverity , @ErrorState);
             IF @TranCounter = 0
                 BEGIN
                     IF XACT_STATE() = 1
                         BEGIN
                             ROLLBACK TRANSACTION;
                         END;
                 END;
             ELSE
                 BEGIN
                
                     --If this procedure was executed within an other transaction.
                     IF XACT_STATE() <> -1
                         BEGIN
                             ROLLBACK TRANSACTION ProcedureSave;
                         END;
                 END;
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE proc_createTeam;
GO

CREATE PROCEDURE proc_createTeam
@lidnr1 INT,
@verenigingLid1 VARCHAR(MAX),
@lidnr2 INT,
@verenigingLid2 VARCHAR(MAX),
@poulenr INT,
@indelingnr INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
	BEGIN TRY
		  IF NOT EXISTS (
			 SELECT lidnr FROM lid L
				WHERE L.LEEFTIJDGRENS IN(
				    SELECT LEEFTIJDSCATEGORIE 
				    FROM Poule P 
				    INNER JOIN KLASSEINDELING K 
					   ON P.INDELINGNR = K.INDELINGNR
				    INNER JOIN CRITERIAINKLASSENINDELING CK 
					   ON K.INDELINGNR = CK.INDELINGNR
				    INNER JOIN CRITERIA C 
					   ON CK.CRITERIANR = C.CRITERIANR
				    WHERE POULENR = @poulenr AND 
					     P.INDELINGNR = @indelingnr
			 )
			 AND L.GESLACHT2 IN (
				SELECT GESLACHT 
				FROM Poule P 
				INNER JOIN KLASSEINDELING K 
				    ON P.INDELINGNR = K.INDELINGNR
				INNER JOIN CRITERIAINKLASSENINDELING CK 
				    ON K.INDELINGNR = CK.INDELINGNR
				INNER JOIN CRITERIA C 
				    ON CK.CRITERIANR = C.CRITERIANR
				WHERE POULENR = @poulenr AND 
					 P.INDELINGNR = @indelingnr
			 )
			 AND L.KLASSECODE IN (
				SELECT KLASSECODE 
				FROM Poule P 
				INNER JOIN KLASSEINDELING K 
				    ON P.INDELINGNR = K.INDELINGNR
				INNER JOIN CRITERIAINKLASSENINDELING CK 
				    ON K.INDELINGNR = CK.INDELINGNR
				INNER JOIN CRITERIA C 
				    ON CK.CRITERIANR = C.CRITERIANR
				WHERE POULENR = @poulenr AND 
				      P.INDELINGNR = @indelingnr
			 )
			 AND L.LIDNR = @lidnr1 AND 
				L.VERENIGINGCODE = @verenigingLid1
		  )
		  BEGIN
			 DECLARE @errormsg VARCHAR(MAX)= 'Lid 1 voldoet niet aan de criteria';
			 THROW 50004, @errormsg, 1;
		  END

		  IF @lidnr2 IS NOT NULL AND @verenigingLid1 IS NOT NULL
		  IF NOT EXISTS
		  (
			SELECT lidnr FROM lid L
			WHERE L.LEEFTIJDGRENS IN(
				SELECT LEEFTIJDSCATEGORIE 
				FROM Poule P 
				INNER JOIN KLASSEINDELING K 
				    ON P.INDELINGNR = K.INDELINGNR
				INNER JOIN CRITERIAINKLASSENINDELING CK 
				    ON K.INDELINGNR = CK.INDELINGNR
				INNER JOIN CRITERIA C 
				    ON CK.CRITERIANR = C.CRITERIANR
				WHERE POULENR = @poulenr AND 
				      P.INDELINGNR = @indelingnr
			 )
			 AND L.GESLACHT2 IN (
				SELECT GESLACHT 
				FROM Poule P 
				INNER JOIN KLASSEINDELING K 
				    ON P.INDELINGNR = K.INDELINGNR
				INNER JOIN CRITERIAINKLASSENINDELING CK 
				    ON K.INDELINGNR = CK.INDELINGNR
				INNER JOIN CRITERIA C 
				    ON CK.CRITERIANR = C.CRITERIANR
				WHERE POULENR = @poulenr AND 
				      P.INDELINGNR = @indelingnr
			 )
			 AND L.KLASSECODE IN (
				SELECT KLASSECODE 
				FROM Poule P 
				INNER JOIN KLASSEINDELING K 
				    ON P.INDELINGNR = K.INDELINGNR
				INNER JOIN CRITERIAINKLASSENINDELING CK 
				    ON K.INDELINGNR = CK.INDELINGNR
				INNER JOIN CRITERIA C 
				    ON CK.CRITERIANR = C.CRITERIANR
				WHERE POULENR = @poulenr AND 
				      P.INDELINGNR = @indelingnr
			 )
			 AND L.LIDNR = @lidnr2 AND
				L.VERENIGINGCODE = @verenigingLid2
		  )
		  BEGIN
			 DECLARE @errormsg2 VARCHAR(MAX)= 'Lid 2 voldoet niet aan de criteria';
			 THROW 50004, @errormsg2, 1;
		  END
		  IF EXISTS (
		 
		  ----Check dubbel team
		  (SELECT 1 from TEAM T
		   WHERE
		    (T.LID2_VERENIGINGCODE = @verenigingLid2 AND T.LID2_LIDNR = @lidnr2
		   AND T.LID1_VERENIGINGCODE = @verenigingLid1 AND T.LID1_LIDNR = @lidnr1)

		   OR

		   (T.LID2_VERENIGINGCODE = @verenigingLid1 AND T.LID2_LIDNR = @lidnr1
		    AND T.LID1_VERENIGINGCODE = @verenigingLid2 AND T.LID1_LIDNR = @lidnr2)
			))
		  
		  BEGIN

		  DECLARE @errormsg3 VARCHAR(MAX)= 'Deze combinatie van spelers bestaat al';
            THROW 50004, @errormsg3, 1;
		  END
		  /*
		  IF (
			(select ISEXTERN from KLASSEINDELING K inner join toernooi T ON K.TOERNOOINR = T.TOERNOOINR inner join TOERNOOIVORM TV ON T.TYPENAAM = TV.TYPENAAM
			WHERE K.INDELINGNR = @indelingnr) = 0
		  )
		  BEGIN
		  
		  IF ((select V.ISEXTERN from VERENIGING V where VERENIGINGCODE = @verenigingLid1) = 1)
		  BEGIN
			DECLARE @errormsg4 VARCHAR(MAX)= 'lid 1 mag niet deelnemen aan dit toernooi, hij behoort niet tot de interne club.';
				THROW 50004, @errormsg3, 1;
			END
		  END

		  */
		  

		  INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD)
		  VALUES(@verenigingLid1, @lidnr1, @verenigingLid2, @lidnr2, @indelingnr, @poulenr, 0, 1)

		  PRINT 'Team toegevoegd'

        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP TRIGGER tr_checkTeamBestaat;
GO

CREATE TRIGGER tr_checkTeamBestaat ON dbo.TEAM
AFTER UPDATE, INSERT
AS
BEGIN
	IF @@ROWCOUNT = 0
	BEGIN
		RETURN;
	END;
	SET NOCOUNT ON;
	BEGIN TRY
		IF EXISTS
		(
			SELECT 1
			FROM inserted AS I
			WHERE EXISTS
			(
				SELECT 1
				FROM dbo.KLASSEINDELING AS KI
					 INNER JOIN
					 dbo.TOERNOOI AS TN
					 ON KI.TOERNOOINR = TN.TOERNOOINR
				WHERE KI.INDELINGNR = I.INDELINGNR AND 
					  TN.TYPENAAM = 'Ladder Toernooi'
			) AND 
				I.LID2_VERENIGINGCODE IS NOT NULL AND
				I.LID2_LIDNR IS NOT NULL
		)
		BEGIN
			DECLARE @errormsg varchar(max)= 'Een dubbelteam kan niet worden aangemaakt voor het Ladder Toernooi type!';
			THROW 50004, @errormsg, 1;
		END;
		ELSE IF EXISTS
			(
				SELECT 1
				FROM inserted AS I
				WHERE EXISTS
				(
					SELECT 1
					FROM dbo.TEAM AS T
					WHERE( I.TEAMNR != T.TEAMNR AND 
						   I.POULENR = T.POULENR AND 
						   I.INDELINGNR = T.INDELINGNR AND 
						   I.LID1_LIDNR = T.LID1_LIDNR AND 
						   I.LID1_VERENIGINGCODE = T.LID1_VERENIGINGCODE AND 
						   I.LID2_LIDNR = T.LID2_LIDNR AND 
						   I.LID2_VERENIGINGCODE = T.LID2_VERENIGINGCODE
						 ) OR 
						 ( I.TEAMNR != T.TEAMNR AND 
						   I.POULENR = T.POULENR AND 
						   I.INDELINGNR = T.INDELINGNR AND 
						   I.LID1_LIDNR = T.LID2_LIDNR AND 
						   I.LID1_VERENIGINGCODE = T.LID2_VERENIGINGCODE AND 
						   I.LID2_LIDNR = T.LID1_LIDNR AND 
						   I.LID2_VERENIGINGCODE = T.LID1_VERENIGINGCODE
						 )
				)
			)
			BEGIN
				DECLARE @errormsg2 varchar(max)= 'Het team bestaat al in het toegewezen toernooi';
				THROW 50005, @errormsg2, 1;
			END;
	END TRY
	BEGIN CATCH
		THROW;
	END CATCH;
END;
GO

DROP TRIGGER trg_tafelCheckOpStartDatumTijd
GO
CREATE TRIGGER trg_tafelCheckOpStartDatumTijd ON Wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF
             ((
			 SELECT count(*)
			 FROM WEDSTRIJD W
			 WHERE W.STARTDATUMTIJD is null
			 AND W.TAFELNR IN (select tafelnr from inserted))
			 > 1
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze tafel is al in gebruik';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE sp_voegWedstrijdToe;
GO

CREATE PROCEDURE sp_voegWedstrijdToe @team1 INT, @team2 INT, @tafelnr INT, @startTime DATETIME
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
		  -- Code here
		  IF EXISTS (
			SELECT * FROM WEDSTRIJD w 
				INNER JOIN TAFEL t 
					ON w.TAFELNR = t.TAFELNR
				WHERE w.STARTDATUMTIJD < @startTime AND (w.EINDDATUMTIJD IS NULL OR w.EINDDATUMTIJD > @startTime)
		  )BEGIN
			 DECLARE @errormsg VARCHAR(MAX)= 'Er is al een wedstrijd bezig op deze tafel.';
                THROW 50004, @errormsg, 1;
		  END
		  ELSE
		  BEGIN
			 INSERT INTO WEDSTRIJD(TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, STARTDATUMTIJD)
				VALUES(@team1, @team2, (SELECT TOP 1 i.INDELINGNR
										  FROM KLASSEINDELING i
										  INNER JOIN POULE p 
											 ON p.INDELINGNR = i.INDELINGNR 
										  INNER JOIN TEAM t 
											 ON p.POULENR = t.POULENR
										  WHERE t.TEAMNR = @team1), 
								   (SELECT TOP 1 p.POULENR 
										  FROM POULE p INNER JOIN TEAM t 
											 ON p.POULENR = t.POULENR
										  WHERE t.TEAMNR = @team1), 
					  @tafelnr, @startTime);
		  END
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

ALTER TABLE SETSCORE DROP CONSTRAINT CK_SETSCORE_SCORE  
GO

ALTER TABLE SETSCORE
ADD CONSTRAINT CK_SETSCORE_SCORE CHECK(team1score >= 0 AND team2score >= 0);
GO

DROP PROC start_kampioenschap
GO
CREATE PROCEDURE start_kampioenschap(
	   @toernooinr int, @poulefaseAantalSets int)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET NOCOUNT, XACT_ABORT ON;
	DECLARE @TranCounter int;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	BEGIN
		SAVE TRANSACTION ProcedureSave;
	END;
	ELSE
	BEGIN
		BEGIN TRANSACTION;
	END;
	BEGIN TRY
		-- Code here
		IF EXISTS
		(
			SELECT 1
			FROM POULE AS P
				 INNER JOIN
				 KLASSEINDELING AS K
				 ON K.INDELINGNR = P.INDELINGNR
			WHERE TOERNOOINR = @toernooinr AND 
				  P.RONDENR > 0
		)
		BEGIN
			RAISERROR('Het toernooi is al gestart of afgelopen', 16, 1);
		END;
--Delete teams
DELETE TEAM
FROM TEAM T
	 INNER JOIN
	 KLASSEINDELING KI
	 ON KI.INDELINGNR = T.INDELINGNR
WHERE KI.TOERNOOINR = @toernooinr AND 
	  T.HEEFTBETAALD = 0;
		IF EXISTS
		(
			SELECT 1
			FROM POULE AS P
				 INNER JOIN
				 KLASSEINDELING AS KI
				 ON KI.INDELINGNR = P.INDELINGNR
			WHERE KI.TOERNOOINR = @toernooinr AND 
			(
				SELECT COUNT(*)
				FROM TEAM
				WHERE POULENR = P.POULENR AND 
					  INDELINGNR = P.INDELINGNR
			) < 2
		)
		BEGIN
			RAISERROR('Er zijn na het verwijderen van teams, die nog niet betaald hebben, inschrijfpoules met minder dan 2 teams', 16, 1);
		END;

		-- Start generating poules
		WHILE(
			 (
				 SELECT COUNT(*)
				 FROM dbo.KLASSEINDELING AS KI
				 WHERE KI.TOERNOOINR = @toernooinr AND 
					   NOT EXISTS
				 (
					 SELECT 1
					 FROM dbo.POULE
					 WHERE POULE.INDELINGNR = KI.INDELINGNR AND 
						   POULE.RONDENR = 1
				 )
			 ) > 0)
		BEGIN
			-- selecting the first 'klasseindeling' with no poules for the poulefase.
			DECLARE @nextKi int=
			(
				SELECT TOP 1 KI.INDELINGNR
				FROM dbo.KLASSEINDELING AS KI
				WHERE KI.TOERNOOINR = @toernooinr AND 
					  NOT EXISTS
				(
					SELECT 1
					FROM dbo.POULE
					WHERE POULE.INDELINGNR = KI.INDELINGNR AND 
						  POULE.RONDENR = 1
				)
			);
			--calculating the number of poules of the poulefast for the certain 'klasseindeling'
			DECLARE @numPoules int= CEILING(CAST(
												(
													SELECT COUNT(*)
													FROM dbo.team
													WHERE team.INDELINGNR = @nextKi
												) AS float) / 4);
			--Generating the correct amount of poules
			WITH createKis(n)
				 AS (
				 SELECT 1
				 UNION ALL
				 SELECT createKis.n + 1
				 FROM createKis
				 WHERE createKis.n < @numPoules -- how many times to iterate
				 )
				 INSERT INTO dbo.POULE( INDELINGNR, NUMMERVANSETS, RONDENR )
						SELECT @nextKi, @poulefaseAantalSets, 1
						FROM createKis;

			-- devide the teams between the available poules
			DECLARE @CurrentPoule int= ( SCOPE_IDENTITY() - @numPoules + 1 );

			UPDATE TEAM
			  SET TEAM.POULENR = TEAM_B.POULENR
			FROM TEAM AS Table_A
				 INNER JOIN
			(
				SELECT t.TEAMNR, ( ROW_NUMBER() OVER(ORDER BY t.TEAMNR) %
								 @numPoules + @CurrentPoule ) AS POULENR
				FROM TEAM AS t
					 INNER JOIN
					 POULE AS p
					 ON t.POULENR = p.POULENR
					 INNER JOIN
					 KLASSEINDELING AS k
					 ON k.INDELINGNR = p.INDELINGNR
					 INNER JOIN
					 TOERNOOI AS te
					 ON te.TOERNOOINR = k.TOERNOOINR
				WHERE k.INDELINGNR = @nextKi AND 
					  te.TOERNOOINR = @toernooinr
			) AS TEAM_B
				 ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			-- Generate matches for every iteration for poule
			WHILE(
				 (
					 SELECT COUNT(*)
					 FROM POULE AS P
					 WHERE POULENR NOT IN
					 (
						 SELECT POULENR
						 FROM WEDSTRIJD
						 WHERE POULENR = P.POULENR AND 
							   INDELINGNR = @nextKI
					 ) AND 
						   INDELINGNR = @nextKI AND 
						   RONDENR = 1
				 ) > 0)
			BEGIN
				DECLARE @nextPoule int=
				(
					SELECT TOP 1 P.POULENR
					FROM dbo.POULE AS P
					WHERE P.INDELINGNR = @nextKi AND 
						  RONDENR = 1 AND 
						  NOT EXISTS
					(
						SELECT 1
						FROM dbo.WEDSTRIJD
						WHERE WEDSTRIJD.POULENR = P.POULENR
					)
				);

				--Inserting matches for the current selected poule
				INSERT INTO dbo.WEDSTRIJD( INDELINGNR, POULENR, TEAMNR1, TEAMNR2 )
					   SELECT @nextKi, @nextPoule, t1.teamnr, t2.teamnr
					   FROM dbo.TEAM AS t1, dbo.TEAM AS t2
					   WHERE t1.TEAMNR <> t2.TEAMNR AND 
							 t2.TEAMNR <> t1.TEAMNR AND 
							 t1.POULENR = @nextPoule AND 
							 t2.POULENR = @nextPoule
					   EXCEPT
					   SELECT @nextKi, @nextPoule, t1.teamnr, t2.teamnr
					   FROM dbo.TEAM AS t1, dbo.TEAM AS t2
					   WHERE t1.TEAMNR <> t2.TEAMNR AND 
							 t2.TEAMNR <> t1.TEAMNR AND 
							 t1.POULENR = @nextPoule AND 
							 t2.POULENR = @nextPoule AND 
							 T1.TEAMNR < T2.TEAMNR;
			END;
		END;
		IF @TranCounter = 0 AND 
		   XACT_STATE() = 1
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		--Error handling
		DECLARE @ErrorMessage varchar(400), @ErrorSeverity int, @ErrorState int;
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
		RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		IF @TranCounter = 0
		BEGIN
			IF XACT_STATE() = 1
			BEGIN
				ROLLBACK TRANSACTION;
			END;
		END;
		ELSE
		BEGIN
			/*If this procedure was executed within an other transaction*/
			IF XACT_STATE() <> -1
			BEGIN
				ROLLBACK TRANSACTION ProcedureSave;
			END;
		END;
		THROW;
	END CATCH;
END;
GO

ALTER TABLE POULE
DROP CONSTRAINT CK_Poule_NummerVanSets
GO
ALTER TABLE POULE
ADD CONSTRAINT CK_Poule_NummerVanSets CHECK(NUMMERVANSETS = 3
                                   OR
                                   NUMMERVANSETS = 5
                                   OR
                                   NUMMERVANSETS = 7);
GO

DROP PROCEDURE sp_createSets 
GO

CREATE PROCEDURE sp_createSets
@Toernooinummer INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
	 DECLARE @AantalSpelers INT;
	 DECLARE @AantalRondes INT;
	 DECLARE @PouleNR INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
	
			SET @AantalSpelers = (select count(*) from team TE
			where TE.POULENR IN
					(
					select P.POULENR
					FROM POULE P INNER JOIN KLASSEINDELING K ON P.INDELINGNR = K.INDELINGNR INNER JOIN TOERNOOI T ON T.TOERNOOINR = K.TOERNOOINR
					WHERE T.TOERNOOINR = @Toernooinummer
					))

					SET @PouleNR = (select P.POULENR
					FROM POULE P INNER JOIN KLASSEINDELING K ON P.INDELINGNR = K.INDELINGNR INNER JOIN TOERNOOI T ON T.TOERNOOINR = K.TOERNOOINR
					WHERE T.TOERNOOINR = @Toernooinummer)

		SET @AantalRondes = LOG(@AantalSpelers)/LOG(2)

		UPDATE POULE SET NUMMERVANSETS = @AantalRondes WHERE POULENR = @POULENR

        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP TRIGGER trg_checkOpLeeftijdscategorie
GO

CREATE TRIGGER trg_checkOpLeeftijdscategorie ON CRITERIAINKLASSENINDELING
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF ((
				select count(distinct leeftijdscategorie) 
				    from criteria C 
				    inner join CRITERIAINKLASSENINDELING CK 
					   ON CK.CRITERIANR = c.CRITERIANR
				    inner join KLASSEINDELING K 
					   on k.INDELINGNR = CK.INDELINGNR 
				    WHERE K.indelingnr IN (select indelingnr from inserted)
			  ) > 1)
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Er mag maar 1 soort leeftijdscategorie toegevoegd worden aan een klasseindeling.';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END
GO

ALTER TABLE LID DROP COLUMN LEEFTIJDGRENS
GO
ALTER TABLE LID
ADD LEEFTIJDGRENS AS CASE
                         WHEN DATEDIFF(YEAR , GEBOORTEDATUM , GETDATE()) < 18
                         THEN 'Junior'
                         ELSE 'Senior'
                     END;


ALTER TABLE LID DROP CONSTRAINT CK_Lid_klasse 
GO

ALTER TABLE LID
ADD CONSTRAINT CK_Lid_klasse CHECK(NTTBNUMMER IS NOT NULL
                                   OR
                                   NTTBNUMMER IS NULL
                                   AND
                                   KLASSECODE = 'I');
GO


DROP TRIGGER tr_checkOfKlasseIndelingAlBestaat
GO

CREATE TRIGGER tr_checkOfKlasseIndelingAlBestaat ON KLASSEINDELING
AFTER INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
			RETURN;
		SET NOCOUNT ON;
		BEGIN TRY
			IF EXISTS
            (
				SELECT t.TOERNOOINR, c.CRITERIANR FROM TOERNOOI t
					INNER JOIN KLASSEINDELING k ON
						t.TOERNOOINR = k.TOERNOOINR
					INNER JOIN CRITERIAINKLASSENINDELING ck ON
						k.INDELINGNR = ck.INDELINGNR
					INNER JOIN CRITERIA c ON
						ck.CRITERIANR = c.CRITERIANR
					GROUP BY t.TOERNOOINR, c.CRITERIANR
					HAVING COUNT(c.CRITERIANR) >1
            )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze klassenindeling bestaat al.';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

DROP PROCEDURE sp_generateRound
GO

CREATE PROCEDURE sp_generateRound @indelingsNr INT, @nrOfSets INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
	   -- Code here============================================================
	   -- Find the highest round number
	   DECLARE @rondenr INT = (SELECT MAX(p.RONDENR) + 0.0 FROM POULE p WHERE p.INDELINGNR = @indelingsNr);

	   -- Does this round have a power of 2 poules?
	   -- Does this round have a power of 2 teams?
	   DECLARE @nrOfPoules FLOAT = (SELECT COUNT(p.POULENR) FROM POULE p WHERE p.INDELINGNR = @indelingsNr AND p.RONDENR = @rondenr)
	   DECLARE @nrOfTeams FLOAT = (SELECT COUNT(t.TEAMNR) FROM POULE p INNER JOIN TEAM t ON p.POULENR = t.POULENR WHERE p.INDELINGNR = @indelingsNr AND p.RONDENR = @rondenr)
	   DECLARE @power FLOAT = LOG(@nrOfPoules) / LOG(2.0)

	   -- How big are the previous poules
	   DECLARE @pouleSize FLOAT = @nrOfTeams / @nrOfPoules;
	   DECLARE @currentPoule INT

	   IF (FLOOR(@power) = CEILING(@power))
	   BEGIN
		  -- Knockout Fase
		  -- If Final
		  IF @nrOfPoules = 2
		  BEGIN
			 PRINT 'Final'
			 -- 2 Poules, 1 with winners and 1 with losers
			 SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p);

			 INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
					 VALUES (@indelingsNr, @nrOfSets, @rondenr + 1),
						   (@indelingsNr, @nrOfSets, @rondenr + 1)

			 -- Update the poules from the winners 
			 UPDATE TEAM SET TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
			 INNER JOIN (
				   SELECT t.TEAMNR, t.SCORE, @currentPoule + 1 AS POULENR FROM TEAM t
				   INNER JOIN POULE p ON p.POULENR = t.POULENR
				   WHERE t.SCORE >= (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
				   AND p.RONDENR = @rondenr				
			 ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR

			 -- Update the poules from the losers
			 UPDATE TEAM SET TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
			 INNER JOIN (
				   SELECT t.TEAMNR, t.SCORE, @currentPoule + 2 AS POULENR FROM TEAM t
				   INNER JOIN POULE p ON p.POULENR = t.POULENR
				   WHERE t.SCORE < (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
				   AND p.RONDENR = @rondenr
			 ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR
		  END
		  ELSE	-- No Final
		  BEGIN
			 
			 SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p);

			 IF @pouleSize > 2
			 BEGIN
				PRINT 'From Poule to Knockout';

				WITH createKis(n)
					AS (
					SELECT 1
					UNION ALL
					SELECT createKis.n + 1
					FROM createKis
					WHERE createKis.n < @nrOfPoules -- how many times to iterate
					)
					INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						    SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						    FROM createKis;

				-- Put the teams in 'random' poules
				UPDATE
				    TEAM
				SET
				    TEAM.POULENR = TEAM_B.POULENR
				FROM TEAM AS Table_A
				    INNER JOIN (
					   SELECT T1.TEAMNR, T1.SCORE, (ROW_NUMBER() OVER (ORDER BY T1.POULENR) % CAST(@nrOfPoules AS INT) + @CurrentPoule + 1) AS POULENR
					   FROM TEAM AS T1
					   INNER JOIN POULE p ON p.POULENR = T1.POULENR
					   WHERE SCORE IN
					   (
						   SELECT TOP 2 SCORE
						   FROM TEAM AS T2
						   WHERE T2.POULENR = T1.POULENR
						   ORDER BY SCORE DESC
					   ) AND 
							TEAMNR IN
					   (
						   SELECT TOP 2 TEAMNR
						   FROM TEAM AS T2
						   WHERE T2.POULENR = T1.POULENR
						   ORDER BY SCORE DESC
					   )
					   AND p.RONDENR = @rondenr
					   ) AS TEAM_B
				ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			 END
			 ELSE
			 BEGIN
				PRINT 'Normal Knockout';
				--Generating the correct amount of poules
				WITH createKis(n)
					AS (
					SELECT 1
					UNION ALL
					SELECT createKis.n + 1
					FROM createKis
					WHERE createKis.n < @nrOfPoules / 2 -- how many times to iterate
					)
					INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						    SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						    FROM createKis;

				UPDATE
				    TEAM
				SET
				    TEAM.POULENR = TEAM_B.POULENR
				FROM TEAM AS Table_A
				    INNER JOIN (
					   SELECT t.TEAMNR, t.SCORE, ntile(CAST(@nrOfPoules / 2 AS INT)) OVER(ORDER BY p.POULENR) + @currentPoule AS POULENR FROM TEAM t
					   INNER JOIN POULE p ON p.POULENR = t.POULENR
					   WHERE t.SCORE >= (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
					   AND p.RONDENR = @rondenr
					   ) AS TEAM_B
				ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			 END
		  END
	   END
	   ELSE
	   BEGIN
		  -- We need to use byes
		  PRINT 'Poule with byes'
		  -- Always poules with 4 teams. Cannot be knockout yet
		  DECLARE @nrOfByes INT = @nrOfPoules - POWER(2, FLOOR(@power))
		  SET @nrOfPoules = POWER(2, FLOOR(@power))
		  SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p) + 1; 

			 --Generating the correct amount of poules
			 WITH createKis(n)
				 AS (
				 SELECT 1
				 UNION ALL
				 SELECT createKis.n + 1
				 FROM createKis
				 WHERE createKis.n < @nrOfPoules -- how many times to iterate
				 )
				 INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						FROM createKis;

			 -- Update the poules with byes
			 UPDATE
				TEAM
			 SET
				TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
				INNER JOIN (
				    SELECT TOP (CAST(@nrOfByes * 4 AS INT)) T1.TEAMNR, T1.SCORE, FLOOR((ROW_NUMBER() OVER(ORDER BY T1.POULENR DESC) - 1) / 4) + @currentPoule AS POULENR
				    FROM TEAM AS T1
				    INNER JOIN POULE p ON p.POULENR = T1.POULENR
				    WHERE SCORE IN
				    (
					    SELECT TOP 2 SCORE
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    ) AND 
						 TEAMNR IN
				    (
					    SELECT TOP 2 TEAMNR
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    )
				    AND p.RONDENR = @rondenr
				    ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR;

			 SET @currentPoule = @currentPoule + @nrOfByes

			 -- Update the poules without byes
			 UPDATE
				TEAM
			 SET
				TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
				INNER JOIN (
				    SELECT TOP (CAST((@nrOfTeams / 2) - (@nrOfByes * 4) AS INT)) T1.TEAMNR, T1.SCORE, FLOOR((ROW_NUMBER() OVER(ORDER BY T1.POULENR ASC) - 1) / 2) + @currentPoule AS POULENR
				    FROM TEAM AS T1
				    INNER JOIN POULE p ON p.POULENR = T1.POULENR
				    WHERE SCORE IN
				    (
					    SELECT TOP 2 SCORE
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    ) AND 
						 TEAMNR IN
				    (
					    SELECT TOP 2 TEAMNR
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    )
				    AND p.RONDENR = @rondenr
				    ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR;
	   END

	   -- Reset the score of all the teams in the new round
	   UPDATE TEAM SET SCORE = 0 WHERE TEAMNR IN (SELECT t.TEAMNR FROM TEAM t 
											 INNER JOIN POULE p ON t.POULENR = p.POULENR 
											 INNER JOIN KLASSEINDELING k ON k.INDELINGNR = p.INDELINGNR
											 WHERE k.INDELINGNR = @indelingsNr);

	   --======================================================================
	   IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

DROP TRIGGER tr_wijzigAdres;
GO

CREATE TRIGGER tr_wijzigAdres ON ADRES
AFTER UPDATE, DELETE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT * FROM inserted i INNER JOIN LID l ON i.HUISNR = l.HUISNR AND i.POSTCODE = l.POSTCODE
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Adres kan niet gewijzigd worden als iemand hier woont.';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO


