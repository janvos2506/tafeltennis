-- CHECK CONSTRAINT TEMPLATE
ALTER TABLE [table]
ADD CONSTRAINT CK_[tabel_name]_[colomn_name] CHECK(--Code here);
GO



-- PROCEDURE TEMPLATE
CREATE PROCEDURE [procedure_name](--params)
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
    BEGIN TRY
		  -- Code here
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO


--TRIGGER TEMPLATE, keep in mind you don't need a transaction
CREATE TRIGGER [trigger_name] ON [target_table]
AFTER -- UPDATE, DELETE INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 -- check code here
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'error message here';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
    