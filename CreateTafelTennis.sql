/*======================================================================================================================================*/
/* TafelTennis Create Scripts			                        															 */
/* Regel 541: timestamp veranderd naar datetime																		 */
/* Regel 1105: FK_TEAM_TEAM_HEEF_LID had voor beide leden dezelde naam dus eentje veranderd naar FK_TEAM_TEAM_HEEF_LID2				 */
/* Regel 1145: FK_WEDSTRIJ_WEDSTRIJD_TEAM winnaar en team hadden dezelfde naam dus winnaar veranderd naar FK_WEDSTRIJ_WEDSTRIJD_WINNAAR */
/*======================================================================================================================================*/
if not exists(select * from sys.databases where name = 'TafelTennis')
create database TafelTennis;
go

use TafelTennis;
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CRITERIA') and o.name = 'FK_CRITERIA_CRITERIA__KLASSE')
alter table CRITERIA
   drop constraint FK_CRITERIA_CRITERIA__KLASSE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CRITERIAINKLASSENINDELING') and o.name = 'FK_CRITERIA_KLASSEIND_CRITERIA')
alter table CRITERIAINKLASSENINDELING
   drop constraint FK_CRITERIA_KLASSEIND_CRITERIA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CRITERIAINKLASSENINDELING') and o.name = 'FK_CRITERIA_KLASSEIND_KLASSEIN')
alter table CRITERIAINKLASSENINDELING
   drop constraint FK_CRITERIA_KLASSEIND_KLASSEIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('KLASSEINDELING') and o.name = 'FK_KLASSEIN_TOERNOOI__TOERNOOI')
alter table KLASSEINDELING
   drop constraint FK_KLASSEIN_TOERNOOI__TOERNOOI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LID') and o.name = 'FK_LID_HEEFT_EEN_ADRES')
alter table LID
   drop constraint FK_LID_HEEFT_EEN_ADRES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LID') and o.name = 'FK_LID_LID_HEEFT_KLASSE')
alter table LID
   drop constraint FK_LID_LID_HEEFT_KLASSE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('LID') and o.name = 'FK_LID_LID_HEEFT_VERENIGI')
alter table LID
   drop constraint FK_LID_LID_HEEFT_VERENIGI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('POULE') and o.name = 'FK_POULE_POULE_IS__KLASSEIN')
alter table POULE
   drop constraint FK_POULE_POULE_IS__KLASSEIN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SETSCORE') and o.name = 'FK_SETSCORE_WEDSTRIJD_WEDSTRIJ')
alter table SETSCORE
   drop constraint FK_SETSCORE_WEDSTRIJD_WEDSTRIJ
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TAFELINTOERNOOI') and o.name = 'FK_TAFELINT_TAFELINTO_TAFEL')
alter table TAFELINTOERNOOI
   drop constraint FK_TAFELINT_TAFELINTO_TAFEL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TAFELINTOERNOOI') and o.name = 'FK_TAFELINT_TAFELINTO_TOERNOOI')
alter table TAFELINTOERNOOI
   drop constraint FK_TAFELINT_TAFELINTO_TOERNOOI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TEAM') and o.name = 'FK_TEAM_TEAM_HEEF_LID2')
alter table TEAM
   drop constraint FK_TEAM_TEAM_HEEF_LID2
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TEAM') and o.name = 'FK_TEAM_TEAM_HEEF_LID')
alter table TEAM
   drop constraint FK_TEAM_TEAM_HEEF_LID
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TEAM') and o.name = 'FK_TEAM_TEAM_IS_I_POULE')
alter table TEAM
   drop constraint FK_TEAM_TEAM_IS_I_POULE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TOERNOOI') and o.name = 'FK_TOERNOOI_HEEFT_ADR_ADRES')
alter table TOERNOOI
   drop constraint FK_TOERNOOI_HEEFT_ADR_ADRES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TOERNOOI') and o.name = 'FK_TOERNOOI_TOERNOOI__TOERNOOI')
alter table TOERNOOI
   drop constraint FK_TOERNOOI_TOERNOOI__TOERNOOI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WEDSTRIJD') and o.name = 'FK_WEDSTRIJ_RELATIONS_TEAM')
alter table WEDSTRIJD
   drop constraint FK_WEDSTRIJ_RELATIONS_TEAM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WEDSTRIJD') and o.name = 'FK_WEDSTRIJ_WEDSTRIJD_TAFEL')
alter table WEDSTRIJD
   drop constraint FK_WEDSTRIJ_WEDSTRIJD_TAFEL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WEDSTRIJD') and o.name = 'FK_WEDSTRIJ_WEDSTRIJD_TEAM')
alter table WEDSTRIJD
   drop constraint FK_WEDSTRIJ_WEDSTRIJD_TEAM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WEDSTRIJD') and o.name = 'FK_WEDSTRIJ_WEDSTRIJD_WINNAAR')
alter table WEDSTRIJD
   drop constraint FK_WEDSTRIJ_WEDSTRIJD_WINNAAR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('WEDSTRIJD') and o.name = 'FK_WEDSTRIJ_WEDSTRIJD_POULE')
alter table WEDSTRIJD
   drop constraint FK_WEDSTRIJ_WEDSTRIJD_POULE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ADRES')
            and   type = 'U')
   drop table ADRES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CRITERIA')
            and   name  = 'CRITERIA_HEEFT_KLASSE_FK'
            and   indid > 0
            and   indid < 255)
   drop index CRITERIA.CRITERIA_HEEFT_KLASSE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CRITERIA')
            and   type = 'U')
   drop table CRITERIA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CRITERIAINKLASSENINDELING')
            and   name  = 'KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA2_FK'
            and   indid > 0
            and   indid < 255)
   drop index CRITERIAINKLASSENINDELING.KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CRITERIAINKLASSENINDELING')
            and   name  = 'KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA_FK'
            and   indid > 0
            and   indid < 255)
   drop index CRITERIAINKLASSENINDELING.KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CRITERIAINKLASSENINDELING')
            and   type = 'U')
   drop table CRITERIAINKLASSENINDELING
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KLASSE')
            and   type = 'U')
   drop table KLASSE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('KLASSEINDELING')
            and   name  = 'TOERNOOI_IS_INGEDEEL_IN_KLASSEINDELING_FK'
            and   indid > 0
            and   indid < 255)
   drop index KLASSEINDELING.TOERNOOI_IS_INGEDEEL_IN_KLASSEINDELING_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KLASSEINDELING')
            and   type = 'U')
   drop table KLASSEINDELING
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LID')
            and   name  = 'LID_HEEFT_EEN_KLASSE_FK'
            and   indid > 0
            and   indid < 255)
   drop index LID.LID_HEEFT_EEN_KLASSE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LID')
            and   name  = 'HEEFT_EEN_ADRES_FK'
            and   indid > 0
            and   indid < 255)
   drop index LID.HEEFT_EEN_ADRES_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('LID')
            and   name  = 'LID_HEEFT_EEN_VERENIGING_FK'
            and   indid > 0
            and   indid < 255)
   drop index LID.LID_HEEFT_EEN_VERENIGING_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LID')
            and   type = 'U')
   drop table LID
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('POULE')
            and   name  = 'POULE_IS_INGEDEELD_OP_KLASSEINDELING_FK'
            and   indid > 0
            and   indid < 255)
   drop index POULE.POULE_IS_INGEDEELD_OP_KLASSEINDELING_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('POULE')
            and   type = 'U')
   drop table POULE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SETSCORE')
            and   name  = 'WEDSTRIJD_BESTAAT_UIT_SETS_FK'
            and   indid > 0
            and   indid < 255)
   drop index SETSCORE.WEDSTRIJD_BESTAAT_UIT_SETS_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SETSCORE')
            and   type = 'U')
   drop table SETSCORE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAFEL')
            and   type = 'U')
   drop table TAFEL
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TAFELINTOERNOOI')
            and   name  = 'TAFELINTOERNOOI2_FK'
            and   indid > 0
            and   indid < 255)
   drop index TAFELINTOERNOOI.TAFELINTOERNOOI2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TAFELINTOERNOOI')
            and   name  = 'TAFELINTOERNOOI_FK'
            and   indid > 0
            and   indid < 255)
   drop index TAFELINTOERNOOI.TAFELINTOERNOOI_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAFELINTOERNOOI')
            and   type = 'U')
   drop table TAFELINTOERNOOI
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TEAM')
            and   name  = 'TEAM_IS_INGEDEELD_IN_POULE_FK'
            and   indid > 0
            and   indid < 255)
   drop index TEAM.TEAM_IS_INGEDEELD_IN_POULE_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TEAM')
            and   name  = 'TEAM_HEEFT_LID2_FK'
            and   indid > 0
            and   indid < 255)
   drop index TEAM.TEAM_HEEFT_LID2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TEAM')
            and   name  = 'TEAM_HEEFT_LID_FK'
            and   indid > 0
            and   indid < 255)
   drop index TEAM.TEAM_HEEFT_LID_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TEAM')
            and   type = 'U')
   drop table TEAM
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TOERNOOI')
            and   name  = 'HEEFT_ADRES_FK'
            and   indid > 0
            and   indid < 255)
   drop index TOERNOOI.HEEFT_ADRES_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TOERNOOI')
            and   name  = 'TOERNOOI_IS_VAN_HET_TYPE_TOERNOOITYPE_FK'
            and   indid > 0
            and   indid < 255)
   drop index TOERNOOI.TOERNOOI_IS_VAN_HET_TYPE_TOERNOOITYPE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TOERNOOI')
            and   type = 'U')
   drop table TOERNOOI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TOERNOOIVORM')
            and   type = 'U')
   drop table TOERNOOIVORM
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VERENIGING')
            and   type = 'U')
   drop table VERENIGING
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WEDSTRIJD')
            and   name  = 'RELATIONSHIP_19_FK'
            and   indid > 0
            and   indid < 255)
   drop index WEDSTRIJD.RELATIONSHIP_19_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WEDSTRIJD')
            and   name  = 'WEDSTRIJD_HEEFT_EEN_TAFEL_FK'
            and   indid > 0
            and   indid < 255)
   drop index WEDSTRIJD.WEDSTRIJD_HEEFT_EEN_TAFEL_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WEDSTRIJD')
            and   name  = 'WEDSTRIJD_HEEFT_TEAM_2_FK'
            and   indid > 0
            and   indid < 255)
   drop index WEDSTRIJD.WEDSTRIJD_HEEFT_TEAM_2_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WEDSTRIJD')
            and   name  = 'WEDSTRIJD_HEEFT_TEAM_1_FK'
            and   indid > 0
            and   indid < 255)
   drop index WEDSTRIJD.WEDSTRIJD_HEEFT_TEAM_1_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('WEDSTRIJD')
            and   name  = 'WEDSTRIJD_HOORT_BIJ_POULE_FK'
            and   indid > 0
            and   indid < 255)
   drop index WEDSTRIJD.WEDSTRIJD_HOORT_BIJ_POULE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('WEDSTRIJD')
            and   type = 'U')
   drop table WEDSTRIJD
go

if exists(select 1 from systypes where name='CODE')
   drop type CODE
go

if exists(select 1 from systypes where name='DATUM')
   drop type DATUM
go

if exists(select 1 from systypes where name='DATUMTIJD')
   drop type DATUMTIJD
go

if exists(select 1 from systypes where name='FLAG')
   drop type FLAG
go

if exists(select 1 from systypes where name='GELD')
   drop type GELD
go

if exists(select 1 from systypes where name='GESLACHT')
   drop type GESLACHT
go

if exists(select 1 from systypes where name='KLASSE')
   drop type KLASSE
go

if exists(select 1 from systypes where name='LAND')
   drop type LAND
go

if exists(select 1 from systypes where name='LEEFTIJDCATEGORIE')
   drop type LEEFTIJDCATEGORIE
go

if exists(select 1 from systypes where name='NAAM')
   drop type NAAM
go

if exists(select 1 from systypes where name='NUMMER')
   drop type NUMMER
go

if exists(select 1 from systypes where name='PLAATS')
   drop type PLAATS
go

if exists(select 1 from systypes where name='POSTCODE')
   drop type POSTCODE
go

if exists(select 1 from systypes where name='SCORE')
   drop type SCORE
go

if exists(select 1 from systypes where name='STRAAT')
   drop type STRAAT
go

if exists(select 1 from systypes where name='TELEFOON')
   drop type TELEFOON
go

if exists(select 1 from systypes where name='UNIEKNUMMER')
   drop type UNIEKNUMMER
go

/*==============================================================*/
/* Domain: CODE                                                 */
/*==============================================================*/
create type CODE
   from char(256)
go

/*==============================================================*/
/* Domain: DATUM                                                */
/*==============================================================*/
create type DATUM
   from date
go

/*==============================================================*/
/* Domain: DATUMTIJD                                            */
/*==============================================================*/
create type DATUMTIJD
   from datetime
go

/*==============================================================*/
/* Domain: FLAG                                                 */
/*==============================================================*/
create type FLAG
   from smallint
go

/*==============================================================*/
/* Domain: GELD                                                 */
/*==============================================================*/
create type GELD
   from numeric(8,2)
go

/*==============================================================*/
/* Domain: GESLACHT                                             */
/*==============================================================*/
create type GESLACHT
   from char(1)
go

/*==============================================================*/
/* Domain: KLASSE                                               */
/*==============================================================*/
create type KLASSE
   from char(1)
go

/*==============================================================*/
/* Domain: LAND                                                 */
/*==============================================================*/
create type LAND
   from char(255)
go

/*==============================================================*/
/* Domain: LEEFTIJDCATEGORIE                                    */
/*==============================================================*/
create type LEEFTIJDCATEGORIE
   from char(256)
go

/*==============================================================*/
/* Domain: NAAM                                                 */
/*==============================================================*/
create type NAAM
   from char(256)
go

/*==============================================================*/
/* Domain: NUMMER                                               */
/*==============================================================*/
create type NUMMER
   from int
go

/*==============================================================*/
/* Domain: PLAATS                                               */
/*==============================================================*/
create type PLAATS
   from char(255)
go

/*==============================================================*/
/* Domain: POSTCODE                                             */
/*==============================================================*/
create type POSTCODE
   from char(10)
go

/*==============================================================*/
/* Domain: SCORE                                                */
/*==============================================================*/
create type SCORE
   from int
go

/*==============================================================*/
/* Domain: STRAAT                                               */
/*==============================================================*/
create type STRAAT
   from char(255)
go

/*==============================================================*/
/* Domain: TELEFOON                                             */
/*==============================================================*/
create type TELEFOON
   from char(10)
go

/*==============================================================*/
/* Domain: UNIEKNUMMER                                          */
/*==============================================================*/
create type UNIEKNUMMER
   from int
go

/*==============================================================*/
/* Table: ADRES                                                 */
/*==============================================================*/
create table ADRES (
   POSTCODE             POSTCODE             not null,
   HUISNR               NUMMER               not null,
   LAND                 LAND                 not null,
   STRAAT               STRAAT               not null,
   PLAATS               PLAATS               not null,
   constraint PK_ADRES primary key (POSTCODE, HUISNR)
)
go

/*==============================================================*/
/* Table: CRITERIA                                              */
/*==============================================================*/
create table CRITERIA (
   CRITERIANR           UNIEKNUMMER         IDENTITY(1,1) not null,
   KLASSECODE           KLASSE               not null,
   LEEFTIJDSCATEGORIE   LEEFTIJDCATEGORIE    not null,
   GESLACHT             GESLACHT             not null,
   constraint PK_CRITERIA primary key (CRITERIANR)
)
go

/*==============================================================*/
/* Index: CRITERIA_HEEFT_KLASSE_FK                              */
/*==============================================================*/




create nonclustered index CRITERIA_HEEFT_KLASSE_FK on CRITERIA (KLASSECODE ASC)
go

/*==============================================================*/
/* Table: CRITERIAINKLASSENINDELING                             */
/*==============================================================*/
create table CRITERIAINKLASSENINDELING (
   CRITERIANR           UNIEKNUMMER          not null,
   INDELINGNR           UNIEKNUMMER          not null,
   constraint PK_CRITERIAINKLASSENINDELING primary key (CRITERIANR, INDELINGNR)
)
go

/*==============================================================*/
/* Index: KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA_FK        */
/*==============================================================*/




create nonclustered index KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA_FK on CRITERIAINKLASSENINDELING (CRITERIANR ASC)
go

/*==============================================================*/
/* Index: KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA2_FK       */
/*==============================================================*/




create nonclustered index KLASSEINDELING_MOET_VOLDOEN_AAN_DE_CRITERIA2_FK on CRITERIAINKLASSENINDELING (INDELINGNR ASC)
go

/*==============================================================*/
/* Table: KLASSE                                                */
/*==============================================================*/
create table KLASSE (
   KLASSECODE           KLASSE               not null,
   constraint PK_KLASSE primary key (KLASSECODE)
)
go

/*==============================================================*/
/* Table: KLASSEINDELING                                        */
/*==============================================================*/
create table KLASSEINDELING (
   INDELINGNR           UNIEKNUMMER          IDENTITY(1,1) not null,
   TOERNOOINR           UNIEKNUMMER          not null,
   DUBBEL               FLAG                 not null,
   constraint PK_KLASSEINDELING primary key (INDELINGNR)
)
go

/*==============================================================*/
/* Index: TOERNOOI_IS_INGEDEEL_IN_KLASSEINDELING_FK             */
/*==============================================================*/




create nonclustered index TOERNOOI_IS_INGEDEEL_IN_KLASSEINDELING_FK on KLASSEINDELING (TOERNOOINR ASC)
go

/*==============================================================*/
/* Table: LID                                                   */
/*==============================================================*/
create table LID (
   VERENIGINGCODE       CODE                 not null,
   LIDNR                UNIEKNUMMER          not null,
   KLASSECODE           KLASSE               not null,
   POSTCODE             POSTCODE             not null,
   HUISNR               NUMMER               not null,
   NTTBNUMMER           UNIEKNUMMER          null,
   VOORNAAM             NAAM                 not null,
   TUSSENVOEGSEL        NAAM                 not null,
   ACHTERNAAM           NAAM                 not null,
   GEBOORTEDATUM        DATUM                not null,
   TELEFOONNUMMER       TELEFOON             not null,
   LEEFTIJDGRENS AS CASE
       WHEN DATEDIFF(YEAR , GEBOORTEDATUM , GETDATE()) < 18
            THEN 'Junior'
            ELSE 'Senior'
       END,
   GESLACHT2            GESLACHT             not null,
   MOBIELNUMMER         TELEFOON             null,
   constraint PK_LID primary key (VERENIGINGCODE, LIDNR)
)
go

/*==============================================================*/
/* Index: LID_HEEFT_EEN_VERENIGING_FK                           */
/*==============================================================*/




create nonclustered index LID_HEEFT_EEN_VERENIGING_FK on LID (VERENIGINGCODE ASC)
go

/*==============================================================*/
/* Index: HEEFT_EEN_ADRES_FK                                    */
/*==============================================================*/




create nonclustered index HEEFT_EEN_ADRES_FK on LID (POSTCODE ASC,
  HUISNR ASC)
go

/*==============================================================*/
/* Index: LID_HEEFT_EEN_KLASSE_FK                               */
/*==============================================================*/




create nonclustered index LID_HEEFT_EEN_KLASSE_FK on LID (KLASSECODE ASC)
go

/*==============================================================*/
/* Table: POULE                                                 */
/*==============================================================*/
create table POULE (
   INDELINGNR           UNIEKNUMMER          not null,
   POULENR              UNIEKNUMMER          IDENTITY(1,1) not null,
   NUMMERVANSETS        NUMMER               not null,
   RONDENR		    NUMMER		     not null,
   constraint PK_POULE primary key (INDELINGNR, POULENR)
)
go

/*==============================================================*/
/* Index: POULE_IS_INGEDEELD_OP_KLASSEINDELING_FK               */
/*==============================================================*/




create nonclustered index POULE_IS_INGEDEELD_OP_KLASSEINDELING_FK on POULE (INDELINGNR ASC)
go

/*==============================================================*/
/* Table: SETSCORE                                              */
/*==============================================================*/
create table SETSCORE (
   WEDSTRIJDNR          UNIEKNUMMER          not null,
   SETNR                NUMMER               not null,
   TEAM1SCORE           SCORE      DEFAULT 0          not null,
   TEAM2SCORE           SCORE      DEFAULT 0          not null,
   constraint PK_SETSCORE primary key (WEDSTRIJDNR, SETNR)
)
go

/*==============================================================*/
/* Index: WEDSTRIJD_BESTAAT_UIT_SETS_FK                         */
/*==============================================================*/




create nonclustered index WEDSTRIJD_BESTAAT_UIT_SETS_FK on SETSCORE (WEDSTRIJDNR ASC)
go

/*==============================================================*/
/* Table: TAFEL                                                 */
/*==============================================================*/
create table TAFEL (
   TAFELNR              UNIEKNUMMER          not null,
   constraint PK_TAFEL primary key (TAFELNR)
)
go

/*==============================================================*/
/* Table: TAFELINTOERNOOI                                       */
/*==============================================================*/
create table TAFELINTOERNOOI (
   TAFELNR              UNIEKNUMMER          not null,
   TOERNOOINR           UNIEKNUMMER          not null,
   constraint PK_TAFELINTOERNOOI primary key (TAFELNR, TOERNOOINR)
)
go

/*==============================================================*/
/* Index: TAFELINTOERNOOI_FK                                    */
/*==============================================================*/




create nonclustered index TAFELINTOERNOOI_FK on TAFELINTOERNOOI (TAFELNR ASC)
go

/*==============================================================*/
/* Index: TAFELINTOERNOOI2_FK                                   */
/*==============================================================*/




create nonclustered index TAFELINTOERNOOI2_FK on TAFELINTOERNOOI (TOERNOOINR ASC)
go

/*==============================================================*/
/* Table: TEAM                                                  */
/*==============================================================*/
create table TEAM (
   TEAMNR               UNIEKNUMMER   IDENTITY(1,1)       not null,
   LID1_VERENIGINGCODE  CODE                 not null,
   LID1_LIDNR           UNIEKNUMMER          not null,
   LID2_VERENIGINGCODE  CODE                 null,
   LID2_LIDNR           UNIEKNUMMER          null,
   INDELINGNR           UNIEKNUMMER          not null,
   POULENR              UNIEKNUMMER          not null,
   SCORE                UNIEKNUMMER          not null,
   HEEFTBETAALD         FLAG                 null,
   WACHTTIJD		    DATUMTIJD		     null,
   constraint PK_TEAM primary key (TEAMNR)
)
go

/*==============================================================*/
/* Index: TEAM_HEEFT_LID_FK                                     */
/*==============================================================*/




create nonclustered index TEAM_HEEFT_LID_FK on TEAM (LID2_VERENIGINGCODE ASC,
  LID2_LIDNR ASC)
go

/*==============================================================*/
/* Index: TEAM_HEEFT_LID2_FK                                    */
/*==============================================================*/




create nonclustered index TEAM_HEEFT_LID2_FK on TEAM (LID1_VERENIGINGCODE ASC,
  LID1_LIDNR ASC)
go

/*==============================================================*/
/* Index: TEAM_IS_INGEDEELD_IN_POULE_FK                         */
/*==============================================================*/




create nonclustered index TEAM_IS_INGEDEELD_IN_POULE_FK on TEAM (INDELINGNR ASC,
  POULENR ASC)
go

/*==============================================================*/
/* Table: TOERNOOI                                              */
/*==============================================================*/
create table TOERNOOI (
   TOERNOOINR           UNIEKNUMMER          IDENTITY(1,1) not null,
   POSTCODE             POSTCODE             not null,
   HUISNR               NUMMER               not null,
   TYPENAAM             NAAM                 not null,
   BEGINDATUMTIJD       DATUMTIJD            not null,
   EINDDATUMTIJD        DATUMTIJD            not null,
   STARTINSCHRIJF       DATUMTIJD            not null,
   EINDINSCHRIJF        DATUMTIJD            not null,
   MINDEELNEMERS        NUMMER               null,
   MAXDEELNEMERS        NUMMER               null,
   INSCHRIJFPRIJS       GELD                 null,
   constraint PK_TOERNOOI primary key (TOERNOOINR)
)
go

/*==============================================================*/
/* Index: TOERNOOI_IS_VAN_HET_TYPE_TOERNOOITYPE_FK              */
/*==============================================================*/




create nonclustered index TOERNOOI_IS_VAN_HET_TYPE_TOERNOOITYPE_FK on TOERNOOI (TYPENAAM ASC)
go

/*==============================================================*/
/* Index: HEEFT_ADRES_FK                                        */
/*==============================================================*/




create nonclustered index HEEFT_ADRES_FK on TOERNOOI (POSTCODE ASC,
  HUISNR ASC)
go

/*==============================================================*/
/* Table: TOERNOOIVORM                                          */
/*==============================================================*/
create table TOERNOOIVORM (
   TYPENAAM             NAAM                 not null,
   ISEXTERN		    BIT			     not null,
   constraint PK_TOERNOOIVORM primary key (TYPENAAM)
)
go

/*==============================================================*/
/* Table: VERENIGING                                            */
/*==============================================================*/
create table VERENIGING (
   VERENIGINGCODE       CODE                 not null,
   VERENIGINGNAAM       NAAM                 not null,
   ISEXTERN		    BIT				not null,
   constraint PK_VERENIGING primary key (VERENIGINGCODE)
)
go

/*==============================================================*/
/* Table: WEDSTRIJD                                             */
/*==============================================================*/
create table WEDSTRIJD (
   WEDSTRIJDNR          UNIEKNUMMER         IDENTITY(1,1) not null,
   TEAMNR1              UNIEKNUMMER          null,
   TEAMNR2              UNIEKNUMMER          null,
   INDELINGNR           UNIEKNUMMER          not null,
   POULENR              UNIEKNUMMER          not null,
   TAFELNR              UNIEKNUMMER          null,
   WINNAAR              UNIEKNUMMER          null,
   STARTDATUMTIJD       DATUMTIJD            null,
   EINDDATUMTIJD        DATUMTIJD            null,
   constraint PK_WEDSTRIJD primary key (WEDSTRIJDNR)
)
go

/*==============================================================*/
/* Index: WEDSTRIJD_HOORT_BIJ_POULE_FK                          */
/*==============================================================*/




create nonclustered index WEDSTRIJD_HOORT_BIJ_POULE_FK on WEDSTRIJD (INDELINGNR ASC,
  POULENR ASC)
go

/*==============================================================*/
/* Index: WEDSTRIJD_HEEFT_TEAM_1_FK                             */
/*==============================================================*/




create nonclustered index WEDSTRIJD_HEEFT_TEAM_1_FK on WEDSTRIJD (TEAMNR2 ASC)
go

/*==============================================================*/
/* Index: WEDSTRIJD_HEEFT_TEAM_2_FK                             */
/*==============================================================*/




create nonclustered index WEDSTRIJD_HEEFT_TEAM_2_FK on WEDSTRIJD (WINNAAR ASC)
go

/*==============================================================*/
/* Index: WEDSTRIJD_HEEFT_EEN_TAFEL_FK                          */
/*==============================================================*/




create nonclustered index WEDSTRIJD_HEEFT_EEN_TAFEL_FK on WEDSTRIJD (TAFELNR ASC)
go

/*==============================================================*/
/* Index: RELATIONSHIP_19_FK                                    */
/*==============================================================*/




create nonclustered index RELATIONSHIP_19_FK on WEDSTRIJD (TEAMNR1 ASC)
go

alter table CRITERIA
   add constraint FK_CRITERIA_CRITERIA__KLASSE foreign key (KLASSECODE)
      references KLASSE (KLASSECODE)
go

alter table CRITERIAINKLASSENINDELING
   add constraint FK_CRITERIA_KLASSEIND_CRITERIA foreign key (CRITERIANR)
      references CRITERIA (CRITERIANR)
go

alter table CRITERIAINKLASSENINDELING
   add constraint FK_CRITERIA_KLASSEIND_KLASSEIN foreign key (INDELINGNR)
      references KLASSEINDELING (INDELINGNR)
go

alter table KLASSEINDELING
   add constraint FK_KLASSEIN_TOERNOOI__TOERNOOI foreign key (TOERNOOINR)
      references TOERNOOI (TOERNOOINR)
go

alter table LID
   add constraint FK_LID_HEEFT_EEN_ADRES foreign key (POSTCODE, HUISNR)
      references ADRES (POSTCODE, HUISNR)
go

alter table LID
   add constraint FK_LID_LID_HEEFT_KLASSE foreign key (KLASSECODE)
      references KLASSE (KLASSECODE)
go

alter table LID
   add constraint FK_LID_LID_HEEFT_VERENIGI foreign key (VERENIGINGCODE)
      references VERENIGING (VERENIGINGCODE)
go

alter table POULE
   add constraint FK_POULE_POULE_IS__KLASSEIN foreign key (INDELINGNR)
      references KLASSEINDELING (INDELINGNR)
go

alter table SETSCORE
   add constraint FK_SETSCORE_WEDSTRIJD_WEDSTRIJ foreign key (WEDSTRIJDNR)
      references WEDSTRIJD (WEDSTRIJDNR)
go

alter table TAFELINTOERNOOI
   add constraint FK_TAFELINT_TAFELINTO_TAFEL foreign key (TAFELNR)
      references TAFEL (TAFELNR)
go

alter table TAFELINTOERNOOI
   add constraint FK_TAFELINT_TAFELINTO_TOERNOOI foreign key (TOERNOOINR)
      references TOERNOOI (TOERNOOINR)
go

alter table TEAM
   add constraint FK_TEAM_TEAM_HEEF_LID2 foreign key (LID2_VERENIGINGCODE, LID2_LIDNR)
      references LID (VERENIGINGCODE, LIDNR)
go

alter table TEAM
   add constraint FK_TEAM_TEAM_HEEF_LID foreign key (LID1_VERENIGINGCODE, LID1_LIDNR)
      references LID (VERENIGINGCODE, LIDNR)
go

alter table TEAM
   add constraint FK_TEAM_TEAM_IS_I_POULE foreign key (INDELINGNR, POULENR)
      references POULE (INDELINGNR, POULENR)
go

alter table TOERNOOI
   add constraint FK_TOERNOOI_HEEFT_ADR_ADRES foreign key (POSTCODE, HUISNR)
      references ADRES (POSTCODE, HUISNR)
go

alter table TOERNOOI
   add constraint FK_TOERNOOI_TOERNOOI__TOERNOOI foreign key (TYPENAAM)
      references TOERNOOIVORM (TYPENAAM)
go

alter table WEDSTRIJD
   add constraint FK_WEDSTRIJ_RELATIONS_TEAM foreign key (TEAMNR1)
      references TEAM (TEAMNR)
go

alter table WEDSTRIJD
   add constraint FK_WEDSTRIJ_WEDSTRIJD_TAFEL foreign key (TAFELNR)
      references TAFEL (TAFELNR)
go

alter table WEDSTRIJD
   add constraint FK_WEDSTRIJ_WEDSTRIJD_TEAM foreign key (TEAMNR2)
      references TEAM (TEAMNR)
go

alter table WEDSTRIJD
   add constraint FK_WEDSTRIJ_WEDSTRIJD_WINNAAR foreign key (WINNAAR)
      references TEAM (TEAMNR)
go

alter table WEDSTRIJD
   add constraint FK_WEDSTRIJ_WEDSTRIJD_POULE foreign key (INDELINGNR, POULENR)
      references POULE (INDELINGNR, POULENR)
go

