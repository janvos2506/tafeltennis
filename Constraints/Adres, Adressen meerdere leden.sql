USE TafelTennis;
GO

-- Constraint
DROP TRIGGER tr_wijzigAdres;
GO

CREATE TRIGGER tr_wijzigAdres ON ADRES
AFTER UPDATE, DELETE
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT * FROM inserted i INNER JOIN LID l ON i.HUISNR = l.HUISNR AND i.POSTCODE = l.POSTCODE
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Adres kan niet gewijzigd worden als iemand hier woont.';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

-- Tests
-- Test AD_1
BEGIN TRAN
PRINT ''
PRINT ''
PRINT 'Expected Error.'
UPDATE ADRES SET PLAATS = 'Schweppenhausen' WHERE POSTCODE = 0010 AND HUISNR = 69;
ROLLBACK
GO

--Test AD_2
BEGIN TRAN
PRINT ''
PRINT ''
PRINT 'Expected Error.'
DELETE FROM ADRES WHERE POSTCODE = 0010 AND HUISNR = 69;
ROLLBACK
GO

-- Test AD_3
BEGIN TRAN
PRINT ''
PRINT ''
PRINT 'Expected Ok.'
INSERT INTO LID ( VERENIGINGCODE , LIDNR , KLASSECODE , POSTCODE , HUISNR , NTTBNUMMER , VOORNAAM , TUSSENVOEGSEL , ACHTERNAAM , GEBOORTEDATUM , TELEFOONNUMMER , GESLACHT2)
VALUES ( 'HVB' , 2000 , 'A' , 1251 , 190 , 234234 , 'Henk' , 'van' , 'henk' , GETDATE() , '026-33363' , 'M'),
( 'HVB' , 2001 , 'A' , 1251 , 190 , 2423426 , 'Henk' , 'van' , 'henk' , GETDATE() , '026-33363' , 'M')
ROLLBACK
GO	