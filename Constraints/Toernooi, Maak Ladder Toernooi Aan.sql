USE TafelTennis;
GO

-- Constraint
DROP PROCEDURE sp_maakLaddertoernooiAan
GO

CREATE PROCEDURE sp_maakLaddertoernooiAan @toernooinr INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
     BEGIN TRY
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
		  -- Code here

		  UPDATE
			 TEAM
		  SET
			 TEAM.SCORE = TEAM_B.SCORE
		  FROM
			 TEAM AS Table_A
			 INNER JOIN (    
				SELECT 1000 - ROW_NUMBER() OVER(ORDER BY l.KLASSECODE DESC) AS SCORE, t.TEAMNR, l.KLASSECODE FROM TEAM t
				    INNER JOIN POULE p
					   ON p.POULENR = t.POULENR
				    INNER JOIN LID l
					   ON l.LIDNR = t.LID1_LIDNR
				    INNER JOIN KLASSEINDELING i
					   ON i.INDELINGNR = p.INDELINGNR
				    INNER JOIN TOERNOOI te
					   ON te.TOERNOOINR = i.TOERNOOINR
				WHERE te.TOERNOOINR = @toernooinr) AS TEAM_B
		  ON Table_A.TEAMNR = TEAM_B.TEAMNR

		  --End Code
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

-- Reset identities
DBCC CHECKIDENT (TOERNOOI, RESEED, 0);		   
DBCC CHECKIDENT (KLASSEINDELING, RESEED, 0);	 
DBCC CHECKIDENT (POULE, RESEED, 0);		  
DBCC CHECKIDENT (TEAM, RESEED, 0);			   
GO

-- Test Data
INSERT INTO TOERNOOI(POSTCODE, HUISNR, TYPENAAM, BEGINDATUMTIJD, EINDDATUMTIJD, STARTINSCHRIJF, EINDINSCHRIJF, MINDEELNEMERS, MAXDEELNEMERS, INSCHRIJFPRIJS)
			VALUES(6106, 219, 'Ladder Toernooi', GETDATE() + 10, GETDATE() + 15, GETDATE() - 5, GETDATE() + 5, 1, 100, 0);
INSERT INTO KLASSEINDELING(TOERNOOINR, DUBBEL)
			VALUES(1, 0)
INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
			VALUES(1, 3, 0)

INSERT INTO TEAM(LID1_LIDNR, LID1_VERENIGINGCODE, LID2_LIDNR, LID2_VERENIGINGCODE, INDELINGNR, POULENR, SCORE, HEEFTBETAALD)
			VALUES	(1, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(2, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(3, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(4, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(5, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(6, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(7, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(8, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(9, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(10, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(11, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(12, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(13, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(14, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(15, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(16, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(17, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(18, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(19, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(20, 'HVB', NULL, NULL, 1, 1, 0, 1);
GO

-- Tests

EXEC sp_maakLaddertoernooiAan @toernooinr = 1
GO

/* Test LT_1: Indeling klopt Klasse laag naar hoog	  					   */
IF EXISTS (
    SELECT te.LID1_LIDNR, te.SCORE, l.KLASSECODE FROM TOERNOOI t
	   INNER JOIN KLASSEINDELING i 
		  ON t.TOERNOOINR = i.TOERNOOINR
	   INNER JOIN POULE p
		  ON p.INDELINGNR = i.INDELINGNR
	   INNER JOIN TEAM te
		  ON te.POULENR = p.POULENR
	   INNER JOIN LID l
		  ON te.LID1_LIDNR = l.LIDNR
	   WHERE EXISTS(
		  SELECT * FROM TOERNOOI t1
			 INNER JOIN KLASSEINDELING i1 
				ON t1.TOERNOOINR = i1.TOERNOOINR
			 INNER JOIN POULE p1
				ON p1.INDELINGNR = i1.INDELINGNR
			 INNER JOIN TEAM te1
				ON te1.POULENR = p1.POULENR
			 INNER JOIN LID l1
				ON te1.LID1_LIDNR = l1.LIDNR
			 WHERE te1.SCORE < te.SCORE AND
				l1.KLASSECODE > l.KLASSECODE
	   )
)
BEGIN
    PRINT 'ERROR: Laddertoernooi is niet goed ingedeeld'
END
ELSE PRINT 'OK: Laddertoernooi is goed ingedeeld'
GO

/* Test LT_2: Geen dubbele scores									  */
IF EXISTS(
    SELECT t.TOERNOOINR, COUNT(te.SCORE) FROM TOERNOOI t
	   INNER JOIN KLASSEINDELING i 
		  ON t.TOERNOOINR = i.TOERNOOINR
	   INNER JOIN POULE p
		  ON p.INDELINGNR = i.INDELINGNR
	   INNER JOIN TEAM te
		  ON te.POULENR = p.POULENR
	   INNER JOIN LID l
		  ON te.LID1_LIDNR = l.LIDNR
	   GROUP BY t.TOERNOOINR, te.SCORE
	   HAVING COUNT(te.SCORE) > 1
)
BEGIN
    PRINT 'ERROR: Er zijn meerdere teams met dezelfde score'
END
ELSE PRINT 'OK: Alle teams hebben een unieke score'
GO

-- Delete inserted test data
DELETE FROM WEDSTRIJD;
DELETE FROM TEAM;
DELETE FROM POULE;
DELETE FROM KLASSEINDELING;
DELETE FROM TOERNOOI;
GO