USE TafelTennis;
GO

-- Constraint
DROP PROCEDURE sp_generateRound
GO

CREATE PROCEDURE sp_generateRound @indelingsNr INT, @nrOfSets INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
    BEGIN TRY
	   -- Code here============================================================
	   -- Find the highest round number
	   DECLARE @rondenr INT = (SELECT MAX(p.RONDENR) + 0.0 FROM POULE p WHERE p.INDELINGNR = @indelingsNr);

	   -- Does this round have a power of 2 poules?
	   -- Does this round have a power of 2 teams?
	   DECLARE @nrOfPoules FLOAT = (SELECT COUNT(p.POULENR) FROM POULE p WHERE p.INDELINGNR = @indelingsNr AND p.RONDENR = @rondenr)
	   DECLARE @nrOfTeams FLOAT = (SELECT COUNT(t.TEAMNR) FROM POULE p INNER JOIN TEAM t ON p.POULENR = t.POULENR WHERE p.INDELINGNR = @indelingsNr AND p.RONDENR = @rondenr)
	   DECLARE @power FLOAT = LOG(@nrOfPoules) / LOG(2.0)

	   -- How big are the previous poules
	   DECLARE @pouleSize FLOAT = @nrOfTeams / @nrOfPoules;
	   DECLARE @currentPoule INT

	   IF (FLOOR(@power) = CEILING(@power))
	   BEGIN
		  -- Knockout Fase
		  -- If Final
		  IF @nrOfPoules = 2
		  BEGIN
			 PRINT 'Final'
			 -- 2 Poules, 1 with winners and 1 with losers
			 SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p);

			 INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
					 VALUES (@indelingsNr, @nrOfSets, @rondenr + 1),
						   (@indelingsNr, @nrOfSets, @rondenr + 1)

			 -- Update the poules from the winners 
			 UPDATE TEAM SET TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
			 INNER JOIN (
				   SELECT t.TEAMNR, t.SCORE, @currentPoule + 1 AS POULENR FROM TEAM t
				   INNER JOIN POULE p ON p.POULENR = t.POULENR
				   WHERE t.SCORE >= (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
				   AND p.RONDENR = @rondenr				
			 ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR

			 -- Update the poules from the losers
			 UPDATE TEAM SET TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
			 INNER JOIN (
				   SELECT t.TEAMNR, t.SCORE, @currentPoule + 2 AS POULENR FROM TEAM t
				   INNER JOIN POULE p ON p.POULENR = t.POULENR
				   WHERE t.SCORE < (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
				   AND p.RONDENR = @rondenr
			 ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR
		  END
		  ELSE	-- No Final
		  BEGIN
			 
			 SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p);

			 IF @pouleSize > 2
			 BEGIN
				PRINT 'From Poule to Knockout';

				WITH createKis(n)
					AS (
					SELECT 1
					UNION ALL
					SELECT createKis.n + 1
					FROM createKis
					WHERE createKis.n < @nrOfPoules -- how many times to iterate
					)
					INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						    SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						    FROM createKis;

				-- Put the teams in 'random' poules
				UPDATE
				    TEAM
				SET
				    TEAM.POULENR = TEAM_B.POULENR
				FROM TEAM AS Table_A
				    INNER JOIN (
					   SELECT T1.TEAMNR, T1.SCORE, (ROW_NUMBER() OVER (ORDER BY T1.POULENR) % CAST(@nrOfPoules AS INT) + @CurrentPoule + 1) AS POULENR
					   FROM TEAM AS T1
					   INNER JOIN POULE p ON p.POULENR = T1.POULENR
					   WHERE SCORE IN
					   (
						   SELECT TOP 2 SCORE
						   FROM TEAM AS T2
						   WHERE T2.POULENR = T1.POULENR
						   ORDER BY SCORE DESC
					   ) AND 
							TEAMNR IN
					   (
						   SELECT TOP 2 TEAMNR
						   FROM TEAM AS T2
						   WHERE T2.POULENR = T1.POULENR
						   ORDER BY SCORE DESC
					   )
					   AND p.RONDENR = @rondenr
					   ) AS TEAM_B
				ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			 END
			 ELSE
			 BEGIN
				PRINT 'Normal Knockout';
				--Generating the correct amount of poules
				WITH createKis(n)
					AS (
					SELECT 1
					UNION ALL
					SELECT createKis.n + 1
					FROM createKis
					WHERE createKis.n < @nrOfPoules / 2 -- how many times to iterate
					)
					INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						    SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						    FROM createKis;

				UPDATE
				    TEAM
				SET
				    TEAM.POULENR = TEAM_B.POULENR
				FROM TEAM AS Table_A
				    INNER JOIN (
					   SELECT t.TEAMNR, t.SCORE, ntile(CAST(@nrOfPoules / 2 AS INT)) OVER(ORDER BY p.POULENR) + @currentPoule AS POULENR FROM TEAM t
					   INNER JOIN POULE p ON p.POULENR = t.POULENR
					   WHERE t.SCORE >= (SELECT MAX(t1.SCORE) FROM TEAM t1 WHERE t.POULENR = t1.POULENR)
					   AND p.RONDENR = @rondenr
					   ) AS TEAM_B
				ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			 END
		  END
	   END
	   ELSE
	   BEGIN
		  -- We need to use byes
		  PRINT 'Poule with byes'
		  -- Always poules with 4 teams. Cannot be knockout yet
		  DECLARE @nrOfByes INT = @nrOfPoules - POWER(2, FLOOR(@power))
		  SET @nrOfPoules = POWER(2, FLOOR(@power))
		  SET @currentPoule = (SELECT MAX(p.POULENR) FROM POULE p) + 1; 

			 --Generating the correct amount of poules
			 WITH createKis(n)
				 AS (
				 SELECT 1
				 UNION ALL
    				 SELECT createKis.n + 1
				 FROM createKis
				 WHERE createKis.n < @nrOfPoules -- how many times to iterate
				 )
				 INSERT INTO dbo.POULE(INDELINGNR, NUMMERVANSETS, RONDENR )
						SELECT @indelingsNr, @nrOfSets, @rondenr + 1
						FROM createKis;

			 -- Update the poules with byes
			 UPDATE
				TEAM
			 SET
				TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
				INNER JOIN (
				    SELECT TOP (CAST(@nrOfByes * 4 AS INT)) T1.TEAMNR, T1.SCORE, FLOOR((ROW_NUMBER() OVER(ORDER BY T1.POULENR DESC) - 1) / 4) + @currentPoule AS POULENR
				    FROM TEAM AS T1
				    INNER JOIN POULE p ON p.POULENR = T1.POULENR
				    WHERE SCORE IN
				    (
					    SELECT TOP 2 SCORE
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    ) AND 
						 TEAMNR IN
				    (
					    SELECT TOP 2 TEAMNR
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    )
				    AND p.RONDENR = @rondenr
				    ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR;

			 SET @currentPoule = @currentPoule + @nrOfByes

			 -- Update the poules without byes
			 UPDATE
				TEAM
			 SET
				TEAM.POULENR = TEAM_B.POULENR
			 FROM TEAM AS Table_A
				INNER JOIN (
				    SELECT TOP (CAST((@nrOfTeams / 2) - (@nrOfByes * 4) AS INT)) T1.TEAMNR, T1.SCORE, FLOOR((ROW_NUMBER() OVER(ORDER BY T1.POULENR ASC) - 1) / 2) + @currentPoule AS POULENR
				    FROM TEAM AS T1
				    INNER JOIN POULE p ON p.POULENR = T1.POULENR
				    WHERE SCORE IN
				    (
					    SELECT TOP 2 SCORE
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    ) AND 
						 TEAMNR IN
				    (
					    SELECT TOP 2 TEAMNR
					    FROM TEAM AS T2
					    WHERE T2.POULENR = T1.POULENR
					    ORDER BY SCORE DESC
				    )
				    AND p.RONDENR = @rondenr
				    ) AS TEAM_B
			 ON Table_A.TEAMNR = TEAM_B.TEAMNR;
	   END

	   -- Reset the score of all the teams in the new round
	   UPDATE TEAM SET SCORE = 0 WHERE TEAMNR IN (SELECT t.TEAMNR FROM TEAM t 
											 INNER JOIN POULE p ON t.POULENR = p.POULENR 
											 INNER JOIN KLASSEINDELING k ON k.INDELINGNR = p.INDELINGNR
											 WHERE k.INDELINGNR = @indelingsNr);

	   --======================================================================
	   IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

-- Setup and tests. We are walking through a tournament
DELETE FROM WEDSTRIJD;
DELETE FROM TEAM;
DELETE FROM POULE;
DELETE FROM KLASSEINDELING;
DELETE FROM TAFELINTOERNOOI;
DELETE FROM TOERNOOI;

DBCC CHECKIDENT (TOERNOOI, RESEED, 0);		   /* Reset auto increment */
DBCC CHECKIDENT (KLASSEINDELING, RESEED, 0);	   /* Reset auto increment */
DBCC CHECKIDENT (POULE, RESEED, 0);		   /* Reset auto increment */
DBCC CHECKIDENT (TEAM, RESEED, 0);			   /* Reset auto increment */

INSERT INTO TOERNOOI(POSTCODE, HUISNR, TYPENAAM, BEGINDATUMTIJD, EINDDATUMTIJD, STARTINSCHRIJF, EINDINSCHRIJF, MINDEELNEMERS, MAXDEELNEMERS, INSCHRIJFPRIJS)
			VALUES(6106, 219, 'Clubkampioenschap', GETDATE() + 10, GETDATE() + 15, GETDATE() - 5, GETDATE() + 5, 1, 100, 0);
INSERT INTO KLASSEINDELING(TOERNOOINR, DUBBEL)
			VALUES(1, 0);

INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
			VALUES(1, 3, 1),
				 (1, 3, 1),
				 (1, 3, 1),
				 (1, 3, 1),
				 (1, 3, 1),
				 (1, 3, 1)

INSERT INTO TEAM(LID1_LIDNR, LID1_VERENIGINGCODE, LID2_LIDNR, LID2_VERENIGINGCODE, INDELINGNR, POULENR, SCORE, HEEFTBETAALD)
			VALUES	(1, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(2, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(3, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(4, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(5, 'HVB', NULL, NULL, 1, 2, 0, 1),
					(6, 'HVB', NULL, NULL, 1, 2, 0, 1),
					(7, 'HVB', NULL, NULL, 1, 2, 0, 1),
					(8, 'HVB', NULL, NULL, 1, 2, 0, 1),
					(9, 'HVB', NULL, NULL, 1, 3, 0, 1),
					(10, 'HVB', NULL, NULL, 1, 3, 0, 1),
					(11, 'HVB', NULL, NULL, 1, 3, 0, 1),
					(12, 'HVB', NULL, NULL, 1, 3, 0, 1),
					(13, 'HVB', NULL, NULL, 1, 4, 0, 1),
					(14, 'HVB', NULL, NULL, 1, 4, 0, 1),
					(15, 'HVB', NULL, NULL, 1, 4, 0, 1),
					(16, 'HVB', NULL, NULL, 1, 4, 0, 1),
					(17, 'HVB', NULL, NULL, 1, 5, 0, 1),
					(18, 'HVB', NULL, NULL, 1, 5, 0, 1),
					(19, 'HVB', NULL, NULL, 1, 5, 0, 1),
					(20, 'HVB', NULL, NULL, 1, 5, 0, 1),
					(21, 'HVB', NULL, NULL, 1, 6, 0, 1),
					(22, 'HVB', NULL, NULL, 1, 6, 0, 1),
					(23, 'HVB', NULL, NULL, 1, 6, 0, 1),
					(24, 'HVB', NULL, NULL, 1, 6, 0, 1);

-- Teams to go to the next round: 1,2,5,6,9,10,13,14,17,18,21,22
UPDATE TEAM SET SCORE = 4 WHERE TEAMNR = 1 OR
						  TEAMNR = 2 OR
						  TEAMNR = 5 OR
						  TEAMNR = 6 OR
						  TEAMNR = 9 OR
						  TEAMNR = 10 OR
						  TEAMNR = 13 OR
						  TEAMNR = 14 OR
						  TEAMNR = 17 OR
						  TEAMNR = 18 OR
						  TEAMNR = 21 OR
						  TEAMNR = 22 
GO

PRINT 'Finishing round. Expecting: Poule with Byes'
EXEC sp_generateRound 1, 3
SELECT t.TEAMNR, p.POULENR, t.SCORE, p.RONDENR FROM TEAM t INNER JOIN POULE p ON p.POULENR = t.POULENR WHERE p.RONDENR = 2 ORDER BY p.POULENR ASC;
GO

-- Teams to go to the next round: 1,2,9,10,17,18,21,22
UPDATE TEAM SET SCORE = 4 WHERE TEAMNR = 9 OR
						  TEAMNR = 10 OR
						  TEAMNR = 17 OR
						  TEAMNR = 18
GO

PRINT 'Finishing round. Expecting: From Poule to Knockout'
EXEC sp_generateRound 1, 3
SELECT t.TEAMNR, p.POULENR, t.SCORE, p.RONDENR FROM TEAM t INNER JOIN POULE p ON p.POULENR = t.POULENR WHERE p.RONDENR = 3 ORDER BY p.POULENR ASC;
GO

-- Teams to go to the next round: 6,1,18,5
UPDATE TEAM SET SCORE = 4 WHERE TEAMNR = 6 OR
						  TEAMNR = 1 OR
						  TEAMNR = 18 OR
						  TEAMNR = 5
GO

PRINT 'Finishing round. Expecting: Normal Knockout'
EXEC sp_generateRound 1, 3
SELECT t.TEAMNR, p.POULENR, t.SCORE, p.RONDENR FROM TEAM t INNER JOIN POULE p ON p.POULENR = t.POULENR WHERE p.RONDENR = 4 ORDER BY p.POULENR ASC;
GO

-- Teams to go to the next round: 6,1,18,5
UPDATE TEAM SET SCORE = 4 WHERE TEAMNR = 6 OR
						  TEAMNR = 5 
GO

PRINT 'Finishing round. Expecting: Final'
EXEC sp_generateRound 1, 3
SELECT t.TEAMNR, p.POULENR, t.SCORE, p.RONDENR FROM TEAM t INNER JOIN POULE p ON p.POULENR = t.POULENR WHERE p.RONDENR = 5 ORDER BY p.POULENR ASC;
GO

-- Delete data left behind
DELETE FROM WEDSTRIJD;
DELETE FROM TEAM;
DELETE FROM POULE;
DELETE FROM KLASSEINDELING;
DELETE FROM TAFELINTOERNOOI;
DELETE FROM TOERNOOI;