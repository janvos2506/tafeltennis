USE TafelTennis;
GO

-- Constraint
DROP PROC sp_afsluitenWedstrijd;
GO

CREATE PROCEDURE sp_afsluitenWedstrijd(
	   @wedstrijdnr int)
AS
BEGIN
	SET NOCOUNT, XACT_ABORT ON;
	DECLARE @TranCounter int;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	BEGIN
		SAVE TRANSACTION ProcedureSave;
	END;
	ELSE
	BEGIN TRANSACTION;
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRY
		--Check wedstrijd al ingepland.
		-- Check of de wedstrijd al is afgesloten
		IF EXISTS
		(
			SELECT 1
			FROM WEDSTRIJD
			WHERE WEDSTRIJDNR = @wedstrijdnr AND 
				  EINDDATUMTIJD IS NOT NULL OR 
				  WINNAAR IS NOT NULL AND 
				  WEDSTRIJDNR = @wedstrijdnr OR 
				  WEDSTRIJDNR = @wedstrijdnr AND 
				  STARTDATUMTIJD IS NULL
		) OR 
		   NOT EXISTS
		(
			SELECT 1
			FROM WEDSTRIJD
			WHERE WEDSTRIJDNR = @wedstrijdnr
		)
		BEGIN
			RAISERROR('De wedstrijd kan niet worden afgesloten of bestaat niet!', 16, 1);
		END;
			--Wedstrijd afsluiten en ranglijst updaten voor laddertoernooi.
		ELSE
		BEGIN
			DECLARE @winnaarRatio int;
			SELECT @winnaarRatio = (
								   (
									   SELECT COUNT(SETNR)
									   FROM SETSCORE AS S
									   WHERE TEAM1SCORE > TEAM2SCORE AND 
											 S.WEDSTRIJDNR = @wedstrijdnr
								   ) -
								   (
									   SELECT COUNT(SETNR)
									   FROM SETSCORE AS S
									   WHERE TEAM2SCORE > TEAM1SCORE AND 
											 S.WEDSTRIJDNR = @wedstrijdnr
								   ) );
			DECLARE @winnaar int;
			DECLARE @winnaarScore int;
			DECLARE @verliezer int;
			DECLARE @verliezerScore int;
			SELECT @winnaar =
			(
				SELECT( CASE
						WHEN @winnaarRatio < 0 THEN TEAMNR2
						WHEN @winnaarRatio > 0 THEN TEAMNR1
						END ) AS WINNAAR
				FROM WEDSTRIJD
				WHERE WEDSTRIJDNR = @wedstrijdnr
			);


			UPDATE WEDSTRIJD
			  SET EINDDATUMTIJD = GETDATE(), WINNAAR = @winnaar
			WHERE WEDSTRIJDNR = @wedstrijdnr;

					    /*
					    - Team wint maar staat hoger dan de verliezer
					    - Team wint en gaat een plek omhoog
					    - Team verliest maar is al lager dan de winnaar
					    - Team verliest en gaat een plek naar beneden
					    */
			IF
			(
				SELECT TYPENAAM
				FROM TOERNOOI
				WHERE TOERNOOINR IN
				(
					SELECT TOERNOOINR
					FROM KLASSEINDELING
					WHERE INDELINGNR IN
					(
						SELECT INDELINGNR
						FROM WEDSTRIJD
						WHERE WEDSTRIJDNR = @wedstrijdnr
					)
				)
			) = ( 'Ladder Toernooi' )
			BEGIN
				SELECT @winnaarScore =
				(
					SELECT SCORE
					FROM TEAM
					WHERE TEAMNR = @winnaar
				);

				SELECT @verliezer =
				(
					SELECT( CASE
							WHEN @winnaarRatio > 0 THEN TEAMNR2
							WHEN @winnaarRatio < 0 THEN TEAMNR1
							END ) AS VERLIEZER
					FROM WEDSTRIJD
					WHERE WEDSTRIJDNR = @wedstrijdnr
				);
				SELECT @verliezerScore =
				(
					SELECT SCORE
					FROM TEAM
					WHERE TEAMNR = @verliezer
				);
				UPDATE TEAM
				  SET SCORE = ( CASE
								WHEN @winnaar = TEAMNR AND 
									 SCORE < @verliezerScore THEN @verliezerScore
								WHEN @verliezer = TEAMNR AND 
									 SCORE > @winnaarScore THEN @winnaarScore
								ELSE SCORE
								END )
				WHERE TEAMNR =
				(
					SELECT TEAMNR1
					FROM WEDSTRIJD
					WHERE WEDSTRIJDNR = @wedstrijdnr
				) OR 
					  TEAMNR =
				(
					SELECT TEAMNR2
					FROM WEDSTRIJD
					WHERE WEDSTRIJDNR = @wedstrijdnr
				);
			END;
			ELSE
			BEGIN
				UPDATE TEAM SET SCORE = SCORE + 1 WHERE TEAMNR = @winnaar
			END;
		END;
		IF @TranCounter = 0 AND 
		   XACT_STATE() = 1
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage varchar(400), @ErrorSeverity int, @ErrorState int;
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
		RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		IF @TranCounter = 0
		BEGIN
			IF XACT_STATE() = 1
			BEGIN
				ROLLBACK TRANSACTION;
			END;
		END;
		ELSE
		BEGIN
                
			--If this procedure was executed within an other transaction.
			IF XACT_STATE() <> -1
			BEGIN
				ROLLBACK TRANSACTION ProcedureSave;
			END;
		END;
		THROW;
	END CATCH;
END;
GO

-- Test Data
-- Maakt een nieuw toernooi aan.
SET IDENTITY_INSERT TOERNOOI ON;

INSERT INTO TOERNOOI ( TOERNOOINR , POSTCODE , HUISNR , BEGINDATUMTIJD , EINDDATUMTIJD , STARTINSCHRIJF , EINDINSCHRIJF , MAXDEELNEMERS , INSCHRIJFPRIJS , TYPENAAM
                     )
VALUES ( 1055 , 1226 , 400 , '2017-05-19' , '2017-07-19' , '2017-05-19' , '2017-07-19' , 30 , 0 , 'Ladder Toernooi'
       );

SET IDENTITY_INSERT TOERNOOI OFF;

-- Maakt nieuwe klassenindeling


SET IDENTITY_INSERT KLASSEINDELING ON;

INSERT INTO KLASSEINDELING ( INDELINGNR , TOERNOOINR , DUBBEL
                           )
VALUES ( 1055 , 1055 , 1
       );

SET IDENTITY_INSERT KLASSEINDELING OFF;

-- Maakt criteria aan


SET IDENTITY_INSERT CRITERIA ON;

INSERT INTO CRITERIA ( CRITERIANR , KLASSECODE , LEEFTIJDSCATEGORIE , GESLACHT
                     )
VALUES ( 1055 , 'B' , 'Junior' , 'M'
       );

INSERT INTO CRITERIA ( CRITERIANR , KLASSECODE , LEEFTIJDSCATEGORIE , GESLACHT
                     )
VALUES ( 1056 , 'C' , 'Junior' , 'M'
       );

SET IDENTITY_INSERT CRITERIA OFF;

-- Voegt criteria aan klasseindeling


INSERT INTO CRITERIAINKLASSENINDELING ( CRITERIANR , INDELINGNR
                                      )
VALUES ( 1055 , 1055
       ), (1056, 1055);

-- Maakt poule aan


SET IDENTITY_INSERT POULE ON;

INSERT INTO POULE ( INDELINGNR , POULENR , NUMMERVANSETS, RONDENR
                  )
VALUES ( 1055 , 1055 , 3, 0
       );

SET IDENTITY_INSERT POULE OFF;

-- Maakt 6 teams aan voor 3 wedstrijden

SET IDENTITY_INSERT TEAM ON;

-- Team 1055


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1055 , 'HVB' , 11 , 1055 , 1055 , 2 , 0
       );

-- Team 1056


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1056 , 'HVB' , 148 , 1055 , 1055 , 1 , 0
       );

-- Team 1057


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1057 , 'HVB' , 271 , 1055 , 1055 , 3 , 0
       );

-- Team 1058


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1058 , 'HVB' , 91 , 1055 , 1055 , 4 , 0
       );

-- Team 1059


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1059 , 'HVB' , 495 , 1055 , 1055 , 5 , 0
       );

-- Team 1060


INSERT INTO TEAM ( TEAMNR , LID1_VERENIGINGCODE , LID1_LIDNR , INDELINGNR , POULENR , SCORE , HEEFTBETAALD
                 )
VALUES ( 1060 , 'HVB' , 91 , 1055 , 1055 , 6 , 0
       );

SET IDENTITY_INSERT TEAM OFF;

--Maakt wedstrijden aan

SET IDENTITY_INSERT WEDSTRIJD ON;

INSERT INTO WEDSTRIJD( WEDSTRIJDNR, TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, STARTDATUMTIJD )
VALUES( 1055, 1055, 1056, 1055, 1055, 5, GETDATE() );

INSERT INTO WEDSTRIJD( WEDSTRIJDNR, TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, STARTDATUMTIJD, WINNAAR, EINDDATUMTIJD )
VALUES( 1056, 1057, 1058, 1055, 1055, 1, GETDATE(), 1058, DATEADD(MINUTE, 15, GETDATE()) );

INSERT INTO WEDSTRIJD( WEDSTRIJDNR, TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, STARTDATUMTIJD )
VALUES( 1057, 1059, 1060, 1055, 1055, 9, NULL );

SET IDENTITY_INSERT WEDSTRIJD OFF;

-- Aanmaken sets + scores voor wedstrijd

--Scores wedstrijd 1055

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1055, 1, 12, 16 );

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1055, 2, 20, 12 );

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1055, 3, 10, 22 );
--Scores wedstrijd 1056


INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1056, 1, 12, 16 );

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1056, 2, 20, 12 );


INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1056, 3, 10, 22 );

-- Score wedstrijd 1057

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1057, 1, 0, 0 );

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1057, 2, 0, 0 );

INSERT INTO SETSCORE( WEDSTRIJDNR, SETNR, TEAM1SCORE, TEAM2SCORE )
VALUES( 1057, 3, 0, 0 );
GO

-- Tests
-- TEST WA_1: Een wedstrijd wordt afgesloten
BEGIN TRAN;
PRINT 'EXPECTED: OK'
EXEC sp_afsluitenWedstrijd 1055
ROLLBACK;

GO
-- TEST WA_2: Een wedstrijd bestaat niet
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
EXEC sp_afsluitenWedstrijd 1432
ROLLBACK

GO
-- TEST WA_3: Een wedstrijd is al afgelopen
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
EXEC sp_afsluitenWedstrijd 1056
ROLLBACK

GO
-- TEST WA_4: Een wedstrijd is nog niet begonnen
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
EXEC sp_afsluitenWedstrijd 1057 
ROLLBACK
GO

-- Delete inserted test data
DELETE FROM SETSCORE
DELETE FROM WEDSTRIJD
DELETE FROM TEAM
DELETE FROM POULE
DELETE FROM CRITERIAINKLASSENINDELING
DELETE FROM CRITERIA
DELETE FROM KLASSEINDELING
DELETE FROM TOERNOOI
GO