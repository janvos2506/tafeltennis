USE TafelTennis;
GO

-- DELETE CONSTRAINT
ALTER TABLE LID
DROP COLUMN LEEFTIJDGRENS;
GO
-- Constraint
ALTER TABLE LID
ADD LEEFTIJDGRENS AS CASE
                         WHEN DATEDIFF(YEAR , GEBOORTEDATUM , GETDATE()) < 18
                         THEN 'Junior'
                         ELSE 'Senior'
                     END;


-- Tests
-- Test LD_1
BEGIN TRAN;
PRINT 'BEGIN TEST 1: Er wordt een lid toegevoegd in de database, met een geboortedatum.';
INSERT INTO LID ( VERENIGINGCODE , LIDNR , KLASSECODE , POSTCODE , HUISNR , NTTBNUMMER , VOORNAAM , TUSSENVOEGSEL , ACHTERNAAM , GEBOORTEDATUM , TELEFOONNUMMER , GESLACHT2)
	    VALUES ( 'HVB' , 10000 , 'A' , 1343, 367, 85484 , 'Henk' , 'van' , 'henk' , GETDATE() , '026-33363' , 'M');

IF EXISTS(SELECT LEEFTIJDGRENS
		  FROM LID
		  WHERE VERENIGINGCODE = 'HVB' AND
			 LIDNR = 10000 AND
			 LEEFTIJDGRENS = 'Junior'
	    ) PRINT 'Test Passed'
   ELSE PRINT 'Test Failed: Expected Junior'
ROLLBACK;

-- Test LD_2
BEGIN TRAN;
PRINT ''
PRINT 'TEST 2: De geboortedrum van ��n lid wordt ge�pdatet.';
UPDATE LID
       SET GEBOORTEDATUM = DATEADD(YEAR , -20 , GETDATE())
WHERE LIDNR = 1
      AND
      VERENIGINGCODE = 'HVB';

IF EXISTS(SELECT LEEFTIJDGRENS
		  FROM LID
		  WHERE VERENIGINGCODE = 'HVB' AND
			 LIDNR = 1 AND
			 LEEFTIJDGRENS = 'Senior'
	    ) PRINT 'Test Passed'
   ELSE PRINT 'Test Failed: Expected Senior'
ROLLBACK;

