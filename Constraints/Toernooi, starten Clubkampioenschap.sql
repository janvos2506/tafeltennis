USE TafelTennis;
GO
-- Constraint
DROP PROC start_kampioenschap
GO

CREATE PROCEDURE start_kampioenschap(
	   @toernooinr int, @poulefaseAantalSets int)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET NOCOUNT, XACT_ABORT ON;
	DECLARE @TranCounter int;
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	BEGIN
		SAVE TRANSACTION ProcedureSave;
	END;
	ELSE
	BEGIN
		BEGIN TRANSACTION;
	END;
	BEGIN TRY
		-- Code here
		IF EXISTS
		(
			SELECT 1
			FROM POULE AS P
				 INNER JOIN
				 KLASSEINDELING AS K
				 ON K.INDELINGNR = P.INDELINGNR
			WHERE TOERNOOINR = @toernooinr AND 
				  P.RONDENR > 0
		)
		BEGIN
			RAISERROR('Het toernooi is al gestart of afgelopen', 16, 1);
		END;
--Delete teams
DELETE TEAM
FROM TEAM T
	 INNER JOIN
	 KLASSEINDELING KI
	 ON KI.INDELINGNR = T.INDELINGNR
WHERE KI.TOERNOOINR = @toernooinr AND 
	  T.HEEFTBETAALD = 0;
		IF EXISTS
		(
			SELECT 1
			FROM POULE AS P
				 INNER JOIN
				 KLASSEINDELING AS KI
				 ON KI.INDELINGNR = P.INDELINGNR
			WHERE KI.TOERNOOINR = @toernooinr AND 
			(
				SELECT COUNT(*)
				FROM TEAM
				WHERE POULENR = P.POULENR AND 
					  INDELINGNR = P.INDELINGNR
			) < 2
		)
		BEGIN
			RAISERROR('Er zijn na het verwijderen van teams, die nog niet betaald hebben, inschrijfpoules met minder dan 2 teams', 16, 1);
		END;

		-- Start generating poules
		WHILE(
			 (
				 SELECT COUNT(*)
				 FROM dbo.KLASSEINDELING AS KI
				 WHERE KI.TOERNOOINR = @toernooinr AND 
					   NOT EXISTS
				 (
					 SELECT 1
					 FROM dbo.POULE
					 WHERE POULE.INDELINGNR = KI.INDELINGNR AND 
						   POULE.RONDENR = 1
				 )
			 ) > 0)
		BEGIN
			-- selecting the first 'klasseindeling' with no poules for the poulefase.
			DECLARE @nextKi int=
			(
				SELECT TOP 1 KI.INDELINGNR
				FROM dbo.KLASSEINDELING AS KI
				WHERE KI.TOERNOOINR = @toernooinr AND 
					  NOT EXISTS
				(
					SELECT 1
					FROM dbo.POULE
					WHERE POULE.INDELINGNR = KI.INDELINGNR AND 
						  POULE.RONDENR = 1
				)
			);
			--calculating the number of poules of the poulefast for the certain 'klasseindeling'
			DECLARE @numPoules int= CEILING(CAST(
												(
													SELECT COUNT(*)
													FROM dbo.team
													WHERE team.INDELINGNR = @nextKi
												) AS float) / 4);
			--Generating the correct amount of poules
			WITH createKis(n)
				 AS (
				 SELECT 1
				 UNION ALL
				 SELECT createKis.n + 1
				 FROM createKis
				 WHERE createKis.n < @numPoules -- how many times to iterate
				 )
				 INSERT INTO dbo.POULE( INDELINGNR, NUMMERVANSETS, RONDENR )
						SELECT @nextKi, @poulefaseAantalSets, 1
						FROM createKis;

			-- devide the teams between the available poules
			DECLARE @CurrentPoule int= ( SCOPE_IDENTITY() - @numPoules + 1 );

			UPDATE TEAM
			  SET TEAM.POULENR = TEAM_B.POULENR
			FROM TEAM AS Table_A
				 INNER JOIN
			(
				SELECT t.TEAMNR, ( ROW_NUMBER() OVER(ORDER BY t.TEAMNR) %
								 @numPoules + @CurrentPoule ) AS POULENR
				FROM TEAM AS t
					 INNER JOIN
					 POULE AS p
					 ON t.POULENR = p.POULENR
					 INNER JOIN
					 KLASSEINDELING AS k
					 ON k.INDELINGNR = p.INDELINGNR
					 INNER JOIN
					 TOERNOOI AS te
					 ON te.TOERNOOINR = k.TOERNOOINR
				WHERE k.INDELINGNR = @nextKi AND 
					  te.TOERNOOINR = @toernooinr
			) AS TEAM_B
				 ON Table_A.TEAMNR = TEAM_B.TEAMNR;
			-- Generate matches for every iteration for poule
			WHILE(
				 (
					 SELECT COUNT(*)
					 FROM POULE AS P
					 WHERE POULENR NOT IN
					 (
						 SELECT POULENR
						 FROM WEDSTRIJD
						 WHERE POULENR = P.POULENR AND 
							   INDELINGNR = @nextKI
					 ) AND 
						   INDELINGNR = @nextKI AND 
						   RONDENR = 1
				 ) > 0)
			BEGIN
				DECLARE @nextPoule int=
				(
					SELECT TOP 1 P.POULENR
					FROM dbo.POULE AS P
					WHERE P.INDELINGNR = @nextKi AND 
						  RONDENR = 1 AND 
						  NOT EXISTS
					(
						SELECT 1
						FROM dbo.WEDSTRIJD
						WHERE WEDSTRIJD.POULENR = P.POULENR
					)
				);

				--Inserting matches for the current selected poule
				INSERT INTO dbo.WEDSTRIJD( INDELINGNR, POULENR, TEAMNR1, TEAMNR2 )
					   SELECT @nextKi, @nextPoule, t1.teamnr, t2.teamnr
					   FROM dbo.TEAM AS t1, dbo.TEAM AS t2
					   WHERE t1.TEAMNR <> t2.TEAMNR AND 
							 t2.TEAMNR <> t1.TEAMNR AND 
							 t1.POULENR = @nextPoule AND 
							 t2.POULENR = @nextPoule
					   EXCEPT
					   SELECT @nextKi, @nextPoule, t1.teamnr, t2.teamnr
					   FROM dbo.TEAM AS t1, dbo.TEAM AS t2
					   WHERE t1.TEAMNR <> t2.TEAMNR AND 
							 t2.TEAMNR <> t1.TEAMNR AND 
							 t1.POULENR = @nextPoule AND 
							 t2.POULENR = @nextPoule AND 
							 T1.TEAMNR < T2.TEAMNR;
			END;
		END;
		IF @TranCounter = 0 AND 
		   XACT_STATE() = 1
		BEGIN
			COMMIT TRANSACTION;
		END;
	END TRY
	BEGIN CATCH
		--Error handling
		DECLARE @ErrorMessage varchar(400), @ErrorSeverity int, @ErrorState int;
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
		RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
		IF @TranCounter = 0
		BEGIN
			IF XACT_STATE() = 1
			BEGIN
				ROLLBACK TRANSACTION;
			END;
		END;
		ELSE
		BEGIN
			/*If this procedure was executed within an other transaction*/
			IF XACT_STATE() <> -1
			BEGIN
				ROLLBACK TRANSACTION ProcedureSave;
			END;
		END;
		THROW;
	END CATCH;
END;
GO
-- Insert test data

-- Maakt een nieuw toernooi aan.
SET IDENTITY_INSERT TOERNOOI ON;

INSERT INTO TOERNOOI( TOERNOOINR, POSTCODE, HUISNR, BEGINDATUMTIJD, EINDDATUMTIJD, STARTINSCHRIJF, EINDINSCHRIJF, MAXDEELNEMERS, INSCHRIJFPRIJS, TYPENAAM )
VALUES( 1055, 1226, 400, '2017-06-01', '2017-07-19', '2017-06-01', '2017-07-19', 30, 0, 'Extern Toernooi' );

SET IDENTITY_INSERT TOERNOOI OFF;

-- Maakt nieuwe klassenindeling

SET IDENTITY_INSERT KLASSEINDELING ON;

INSERT INTO KLASSEINDELING( INDELINGNR, TOERNOOINR, DUBBEL )
VALUES( 1055, 1055, 1 );

INSERT INTO KLASSEINDELING( INDELINGNR, TOERNOOINR, DUBBEL )
VALUES( 1056, 1055, 1 );

SET IDENTITY_INSERT KLASSEINDELING OFF;

-- Maakt criteria aan

SET IDENTITY_INSERT CRITERIA ON;

INSERT INTO CRITERIA( CRITERIANR, KLASSECODE, LEEFTIJDSCATEGORIE, GESLACHT )
VALUES( 1055, 'B', 'Junior', 'M' );

INSERT INTO CRITERIA( CRITERIANR, KLASSECODE, LEEFTIJDSCATEGORIE, GESLACHT )
VALUES( 1056, 'C', 'Junior', 'M' );

SET IDENTITY_INSERT CRITERIA OFF;

-- Voegt criteria aan klasseindeling


INSERT INTO CRITERIAINKLASSENINDELING( CRITERIANR, INDELINGNR )
VALUES( 1055, 1055 ), ( 1056, 1056 );

-- Maakt poule aan


SET IDENTITY_INSERT POULE ON;

INSERT INTO POULE( INDELINGNR, POULENR, NUMMERVANSETS, RONDENR )
VALUES( 1055, 1055, 3, 0);

INSERT INTO POULE( INDELINGNR, POULENR, NUMMERVANSETS, RONDENR )
VALUES( 1056, 1056, 3, 0);

SET IDENTITY_INSERT POULE OFF;

-- Maakt 6 teams aan 

SET IDENTITY_INSERT TEAM ON;

-- Team 1055


INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1055, 'HVB', 11, 1055, 1055, 0, 1 );

-- Team 1056


INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1056, 'HVB', 222, 1055, 1055, 0, 1 );

-- Team 1057

INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1057, 'HVB', 271, 1056, 1056, 0, 1 );

-- Team 1058


INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1058, 'HVB', 91, 1056, 1056, 0, 1 );

-- Team 1059


INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1059, 'HVB', 495, 1056, 1056, 0, 1 );

-- Team 1060


INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1060, 'HVB', 529, 1056, 1056, 0, 1 );

INSERT INTO TEAM( TEAMNR, LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
VALUES( 1061, 'HVB', 699, 1056, 1056, 0, 1 );


SET IDENTITY_INSERT TEAM OFF;
--End test data

GO

-- Tests
-- Test TS_1 : Minder dan twee mensen per inschrijfpoule, teams die niet hebben betaald zijn al verwijderd.
PRINT 'Expected Error'
DBCC CHECKIDENT(POULE, RESEED, 1056);
BEGIN TRAN 
UPDATE TEAM SET HEEFTBETAALD = 0 WHERE TEAMNR = 1055
EXEC start_kampioenschap 1055, 3
ROLLBACK
GO
-- Test TS_2: Toernooi is al gestart oftewel poul indeling is algemaakt.
PRINT 'Expected Error'
DBCC CHECKIDENT(POULE, RESEED, 1056);
BEGIN TRAN 
EXEC start_kampioenschap 1055, 3
EXEC start_kampioenschap 1055, 3
ROLLBACK
GO
-- Test TS_3: Toernooi wordt gestart.
PRINT 'Expected Ok'
DBCC CHECKIDENT(POULE, RESEED, 1056);
BEGIN TRAN 
EXEC start_kampioenschap 1055, 3
ROLLBACK
GO
-- Delete inserted test data
DELETE FROM WEDSTRIJD
DELETE FROM TEAM
DELETE FROM CRITERIAINKLASSENINDELING
DELETE FROM CRITERIA
DELETE FROM POULE
DELETE FROM KLASSEINDELING
DELETE FROM TOERNOOI