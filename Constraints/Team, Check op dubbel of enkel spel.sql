USE TafelTennis;
GO

-- Constraint
DROP TRIGGER tr_checkTeamBestaat;
GO

CREATE TRIGGER tr_checkTeamBestaat ON dbo.TEAM
AFTER UPDATE, INSERT
AS
BEGIN
	IF @@ROWCOUNT = 0
	BEGIN
		RETURN;
	END;
	SET NOCOUNT ON;
	BEGIN TRY
		IF EXISTS
		(
			SELECT 1
			FROM inserted AS I
			WHERE EXISTS
			(
				SELECT 1
				FROM dbo.KLASSEINDELING AS KI
					 INNER JOIN
					 dbo.TOERNOOI AS TN
					 ON KI.TOERNOOINR = TN.TOERNOOINR
				WHERE KI.INDELINGNR = I.INDELINGNR AND 
					  TN.TYPENAAM = 'Ladder Toernooi'
			) AND 
				I.LID2_VERENIGINGCODE IS NOT NULL AND
				I.LID2_LIDNR IS NOT NULL
		)
		BEGIN
			DECLARE @errormsg varchar(max)= 'Een dubbelteam kan niet worden aangemaakt voor het Ladder Toernooi type!';
			THROW 50004, @errormsg, 1;
		END;
		ELSE IF EXISTS
			(
				SELECT 1
				FROM inserted AS I
				WHERE EXISTS
				(
					SELECT 1
					FROM dbo.TEAM AS T
					WHERE( I.TEAMNR != T.TEAMNR AND 
						   I.POULENR = T.POULENR AND 
						   I.INDELINGNR = T.INDELINGNR AND 
						   I.LID1_LIDNR = T.LID1_LIDNR AND 
						   I.LID1_VERENIGINGCODE = T.LID1_VERENIGINGCODE AND 
						   I.LID2_LIDNR = T.LID2_LIDNR AND 
						   I.LID2_VERENIGINGCODE = T.LID2_VERENIGINGCODE
						 ) OR 
						 ( I.TEAMNR != T.TEAMNR AND 
						   I.POULENR = T.POULENR AND 
						   I.INDELINGNR = T.INDELINGNR AND 
						   I.LID1_LIDNR = T.LID2_LIDNR AND 
						   I.LID1_VERENIGINGCODE = T.LID2_VERENIGINGCODE AND 
						   I.LID2_LIDNR = T.LID1_LIDNR AND 
						   I.LID2_VERENIGINGCODE = T.LID1_VERENIGINGCODE
						 )
				)
			)
			BEGIN
				DECLARE @errormsg2 varchar(max)= 'Het team bestaat al in het toegewezen toernooi';
				THROW 50005, @errormsg2, 1;
			END;
	END TRY
	BEGIN CATCH
		THROW;
	END CATCH;
END;
GO

-- Test Data
-- Maakt een nieuw toernooi aan.
SET IDENTITY_INSERT TOERNOOI ON;
INSERT INTO TOERNOOI ( TOERNOOINR , POSTCODE , HUISNR , BEGINDATUMTIJD , EINDDATUMTIJD , STARTINSCHRIJF , EINDINSCHRIJF , MAXDEELNEMERS , INSCHRIJFPRIJS , TYPENAAM)
VALUES ( 1055 , 5216 , 153 , '2017-05-19' , '2017-07-19' , '2017-05-19' , '2017-07-19' , 30 , 0 , 'Ladder Toernooi'
       ), ( 1056 , 5216 , 153 , '2017-05-19' , '2017-07-19' , '2017-05-19' , '2017-07-19' , 30 , 0 , 'Extern Toernooi');
SET IDENTITY_INSERT TOERNOOI OFF;

-- Maakt nieuwe klassenindeling
SET IDENTITY_INSERT KLASSEINDELING ON;
INSERT INTO KLASSEINDELING ( INDELINGNR , TOERNOOINR , DUBBEL)
VALUES ( 1055 , 1055 , 1);

INSERT INTO KLASSEINDELING ( INDELINGNR , TOERNOOINR , DUBBEL)
VALUES ( 1056 , 1056 , 1);
SET IDENTITY_INSERT KLASSEINDELING OFF;

-- Maakt criteria aan
SET IDENTITY_INSERT CRITERIA ON;
INSERT INTO CRITERIA ( CRITERIANR , KLASSECODE , LEEFTIJDSCATEGORIE , GESLACHT)
VALUES ( 1055 , 'B' , 'Junior' , 'M');

INSERT INTO CRITERIA ( CRITERIANR , KLASSECODE , LEEFTIJDSCATEGORIE , GESLACHT)
VALUES ( 1056 , 'C' , 'Junior' , 'M');
SET IDENTITY_INSERT CRITERIA OFF;

-- Voegt criteria aan klasseindeling
INSERT INTO CRITERIAINKLASSENINDELING ( CRITERIANR , INDELINGNR)
VALUES    (1055 , 1055
       ), (1056, 1055
	  ), (1055 , 1056
       ), (1056, 1056);

-- Maakt poule aan
SET IDENTITY_INSERT POULE ON;
INSERT INTO POULE ( INDELINGNR , POULENR , NUMMERVANSETS, RONDENR)
VALUES ( 1055 , 1055 , 3, 0
       ), ( 1056 , 1056 , 3, 0);
SET IDENTITY_INSERT POULE OFF;
GO

-- Tests
-- TEST WD_1: Voegt dubbel team toe aan ladder toernooi.
BEGIN TRAN;
PRINT 'EXPECTED: ERROR';
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1055, 1055, 2, 0 );
ROLLBACK;
GO

-- TEST WD_2: Voegt team toe aan niet ladder toernooi
BEGIN TRAN
PRINT 'EXPECTED: OK'
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1056, 1056, 2, 0 );
ROLLBACK
GO

-- TEST WD_3: Voegt meerdere dubbel teams toe aan ladder toernooi
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1055, 1055, 2, 0 ), 
	     ('HVB', 271, 'HVB', 91, 1055, 1055, 2, 0 );
ROLLBACK
GO

-- TEST WD_4: Voegt meerdere dubbel teams toe aan een dubbeltoernooi
BEGIN TRAN
PRINT 'EXPECTED: OK'
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1056, 1056, 2, 0 ), 
		('HVB', 271, 'HVB', 91, 1056, 1056, 2, 0 );
ROLLBACK
GO

-- TEST WD_5: Voeg 2 keer hetzelfde team toe aan een toernooi
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1056, 1056, 2, 0 );
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1056, 1056, 2, 0 );
ROLLBACK
GO

-- TEST WD_6: Hetzelfde als WD_5 maar dan in ��n insert.
BEGIN TRAN
PRINT 'EXPECTED: ERROR'
INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, LID2_VERENIGINGCODE, LID2_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD )
    VALUES('HVB', 11, 'HVB', 148, 1055, 1055, 2, 0 ), 
		('HVB', 11, 'HVB', 148, 1055, 1055, 2, 0 );
ROLLBACK
GO

-- Delete inserted test data
DELETE FROM POULE
DELETE FROM CRITERIAINKLASSENINDELING
DELETE FROM CRITERIA
DELETE FROM KLASSEINDELING
DELETE FROM TOERNOOI
GO