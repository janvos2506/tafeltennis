

-- Constraint
DROP TRIGGER trg_isWedstrijdNogBezig
GO

CREATE TRIGGER trg_isWedstrijdNogBezig ON Wedstrijd
AFTER UPDATE, INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
             RETURN;
         SET NOCOUNT ON;
         BEGIN TRY
             IF EXISTS
             (
			 SELECT 1 from WEDSTRIJD W
			 WHERE EINDDATUMTIJD is null AND WEDSTRIJDNR NOT IN (select WEDSTRIJDNR from inserted)

			 AND(    W.TEAMNR1 IN (select TEAMNR1 from inserted)
				 OR W.TEAMNR1 IN (select TEAMNR2 from inserted)
				 OR W.TEAMNR2 IN (select TEAMNR1 from inserted)
				 OR W.TEAMNR2 IN (select TEAMNR2 from inserted)
				 )
             )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= '1 van de teams moet nog een wedstrijd spelen';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

-- Test data
BEGIN TRANSACTION T
GO
INSERT INTO ADRES (POSTCODE,HUISNR,LAND,STRAAT,PLAATS) VALUES (6106,219,'Nederland','straat','plaats')
INSERT INTO TOERNOOI(POSTCODE, HUISNR, TYPENAAM, BEGINDATUMTIJD, EINDDATUMTIJD, STARTINSCHRIJF, EINDINSCHRIJF, MINDEELNEMERS, MAXDEELNEMERS, INSCHRIJFPRIJS)
			VALUES(6106, 219, 'Ladder Toernooi', GETDATE() + 10, GETDATE() + 15, GETDATE() - 5, GETDATE() + 5, 1, 100, 0);

INSERT INTO KLASSEINDELING(TOERNOOINR, DUBBEL)
			VALUES(1, 0);

INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
			VALUES(1, 3, 0);

INSERT INTO TEAM(LID1_VERENIGINGCODE, LID1_LIDNR, INDELINGNR, POULENR, SCORE, HEEFTBETAALD)
	     VALUES('HVB', 10, 1, 1, 0, 1),
			 ('HVB', 11, 1, 1, 0, 1);

INSERT INTO WEDSTRIJD (TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, WINNAAR, STARTDATUMTIJD, EINDDATUMTIJD)
		     VALUES (1, 2, 1, 1, 1, null, '5-18-2017', null),
				  (1, 2, 1, 1, 1, null, '5-18-2017', null),
				  (1, 2, 1, 1, 1, null, '5-18-2017', null),
				  (1, 2, 1, 1, 1, null, '5-18-2017', null),
				  (1, 2, 1, 1, 1, 1, '5-17-2017', '5-18-2017');
GO

-- Tests
-- Test WW_1
PRINT 'WW 1 EXPECTED OK'
INSERT INTO WEDSTRIJD (TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, WINNAAR, STARTDATUMTIJD, EINDDATUMTIJD)
VALUES (1, 2, 1, 1, 1, null, '5-18-2017', getdate()),
       (1, 2, 1, 1, 1, null, '5-18-2017', null)
GO

-- Test WW_2
PRINT 'WW 2 EXPECTED ERROR'
INSERT INTO WEDSTRIJD (TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, WINNAAR, STARTDATUMTIJD, EINDDATUMTIJD)
VALUES (1, 2, 1, 1, 1, null, '5-18-2017', null),
       (1, 2, 1, 1, 1, null, '5-18-2017', null)
GO

-- Test WW_3
PRINT 'WW 3 EXPECTED OK'
INSERT INTO WEDSTRIJD (TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, WINNAAR, STARTDATUMTIJD, EINDDATUMTIJD)
VALUES (1, 2, 1, 1, 1, null, '5-18-2017', getdate()),
       (1, 2, 1, 1, 1, null, '6-18-2017', null),
	   (3, 4, 1, 1, 1, null, '5-18-2017', getdate()),
       (3, 4, 1, 1, 1, null, '6-18-2017', null)
GO

-- Test WW_4
PRINT 'WW 4 EXPECTED OK'
INSERT INTO WEDSTRIJD (TEAMNR1, TEAMNR2, INDELINGNR, POULENR, TAFELNR, WINNAAR, STARTDATUMTIJD, EINDDATUMTIJD)
VALUES (1, 2, 1, 1, 1, null, '5-18-2017', getdate()),
       (1, 2, 1, 1, 1, null, '6-18-2017', getdate()),
	   (3, 4, 1, 1, 1, null, '5-18-2017', getdate()),
       (3, 4, 1, 1, 1, null, '6-18-2017', null)
GO

-- Delete inserted test data
ROLLBACK TRANSACTION T
GO