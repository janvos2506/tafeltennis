USE TafelTennis
GO

drop procedure proc_scoreInvullen
GO

CREATE PROCEDURE proc_scoreInvullen
@wedstrijdnr INT,
@team1score INT,
@team2score INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
    BEGIN TRY

	DECLARE @poulenr INT
	DECLARE @sets INT
	DECLARE @countsets INT

	SET @poulenr = (select poulenr from wedstrijd where wedstrijdnr = @wedstrijdnr)
	SET @sets = (select NUMMERVANSETS from poule where poulenr = @poulenr)
	SET @countsets = (select count(*) from setscore where wedstrijdnr = @wedstrijdnr) + 1

		  IF(@countsets < @sets)
		 
		 INSERT INTO SETSCORE
		  VALUES(@wedstrijdnr,@countsets,@team1score,@team2score)

	
		  ELSE

		  
		  EXEC sp_afsluitenWedstrijd @wedstrijdnr
		  

        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO


BEGIN TRANSACTION T
GO
INSERT INTO ADRES
VALUES ('1234','2','Nederlands','Straat','Arnhem'),
	   ('1235','3','Nederlands','Straat','Arnhem'),
	   ('1236','4','Nederlands','Straat','Arnhem')
GO

INSERT INTO LID (VERENIGINGCODE,LIDNR,KLASSECODE,POSTCODE,HUISNR,NTTBNUMMER,VOORNAAM,TUSSENVOEGSEL,ACHTERNAAM,GEBOORTEDATUM,TELEFOONNUMMER,GESLACHT2,MOBIELNUMMER)
VALUES ('HVB',1009,'A','1234','2',0000,'Frits','','Jansen','1980','0677889987','M',null),
	   ('HVB',1008,'A','1235','3',0001,'Freek','','Jansen','1980','0677889987','M',null)
GO

SET IDENTITY_INSERT TOERNOOI ON
INSERT INTO TOERNOOI (TOERNOOINR,POSTCODE,HUISNR,TYPENAAM,BEGINDATUMTIJD,EINDDATUMTIJD,STARTINSCHRIJF,EINDINSCHRIJF,MINDEELNEMERS,MAXDEELNEMERS,INSCHRIJFPRIJS)
VALUES (1005,'1236','4','Ladder toernooi',DATEADD(MONTH,1,GETDATE()),DATEADD(MONTH,2,GETDATE()),DATEADD(MONTH,-1,GETDATE()),DATEADD(MONTH,1,GETDATE()),2,2,0)
SET IDENTITY_INSERT TOERNOOI OFF
GO

SET IDENTITY_INSERT CRITERIA ON
INSERT INTO CRITERIA (CRITERIANR,KLASSECODE,LEEFTIJDSCATEGORIE,GESLACHT)
VALUES (10077,'A','Senior','M')
SET IDENTITY_INSERT CRITERIA OFF
GO

SET IDENTITY_INSERT KLASSEINDELING ON
INSERT INTO KLASSEINDELING (INDELINGNR,TOERNOOINR,DUBBEL)
VALUES (1077,1005,0)
SET IDENTITY_INSERT KLASSEINDELING OFF
GO

SET IDENTITY_INSERT POULE ON
INSERT INTO POULE (INDELINGNR, POULENR,NUMMERVANSETS,RONDENR)
VALUES (1077,1088,3,0)
SET IDENTITY_INSERT POULE OFF
GO

INSERT INTO CRITERIAINKLASSENINDELING
VALUES (10077,1077)
GO

SET IDENTITY_INSERT TEAM ON
INSERT INTO TEAM (TEAMNR, LID1_VERENIGINGCODE,LID1_LIDNR,LID2_VERENIGINGCODE,LID2_LIDNR,INDELINGNR,POULENR,SCORE,HEEFTBETAALD,WACHTTIJD)
VALUES (2055,'HVB',1009,null,null,1077,1088,999,1,null),
	   (2056,'HVB',1008,null,null,1077,1088,998,1,null)
SET IDENTITY_INSERT TEAM OFF
insert into TAFELINTOERNOOI values (5,1005)
GO
SET IDENTITY_INSERT WEDSTRIJD ON
INSERT INTO WEDSTRIJD (WEDSTRIJDNR,TEAMNR1,TEAMNR2,INDELINGNR,POULENR,TAFELNR,WINNAAR,STARTDATUMTIJD,EINDDATUMTIJD) values 
(69696,2055,2056,1077,1088,5,null,getdate(),null)
SET IDENTITY_INSERT WEDSTRIJD OFF
GO

-- Tests
-- Test WE_1
PRINT 'TESTS'
PRINT '----------------------------------'
PRINT 'EXPECTED: scores ingevuld, wedstrijd afgesloten'
exec proc_scoreInvullen 69696,0,1
exec proc_scoreInvullen 69696,0,1
exec proc_scoreInvullen 69696,0,1
GO

-- Test WE_2
PRINT '----------------------------------'
PRINT 'EXPECTED: Error'
exec proc_scoreInvullen 69696, 0, 1
GO

ROLLBACK TRANSACTION T
GO

