USE TafelTennis;
GO

-- Constraint
DROP PROC proc_createWedstrijdLaddertoernooi
GO
CREATE PROCEDURE proc_createWedstrijdLaddertoernooi
@team1 INT,
@team2 INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
		      ELSE
     BEGIN TRANSACTION;
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
    BEGIN TRY

	DECLARE @indelingnr INT
	DECLARE @poulenr INT
	DECLARE @tafelnr INT
	DECLARE @toernooirnr INT
	DECLARE @lastTimePlayed DATETIME

	SET @indelingnr = (select indelingnr from team where teamnr = @team1)
	SET @poulenr = (select poulenr from team where teamnr = @team1)
	SET @toernooirnr = (select toernooinr from klasseindeling where indelingnr = @indelingnr)
	SET @tafelnr = (select tafelnr from tafelintoernooi where toernooinr = @toernooirnr)
	SET @lastTimePlayed = ( SELECT TOP 1 EindDatumTijd from wedstrijd W
							WHERE (Teamnr1 = @team1 AND teamnr2 = @team2)
							   OR (Teamnr1 = @team2 AND teamnr1 = @team2)
							ORDER BY EindDatumTijd DESC)
							
	IF (@lastTimePlayed > dateadd(MONTH, -2, getDATE()))
			
	BEGIN
					 DECLARE @errormsg VARCHAR(MAX)= 'Deze twee teams mogen nog geen wedstrijd spelen. Er moet minimaal twee maanden wachttijd tussen zitten.';
                     THROW 50004, @errormsg, 1;
	END

	DECLARE @team1Score INT
	DECLARE @team2Score INT

	SET @team1Score = (select score from team where teamnr = @team1)
	SET @team2Score = (select score from team where teamnr = @team2)

	IF (@team1Score < (@team2score -3)) OR (@team1Score > (@team2score + 3))

	BEGIN
					 DECLARE @errormsg2 VARCHAR(MAX)= 'Deze twee teams mogen geen wedstrijd spelen. Hun rang ligt te ver uit elkaar.';
                     THROW 50004, @errormsg2, 1;
	END

		  insert into wedstrijd (teamnr1, teamnr2, indelingnr, poulenr, tafelnr, winnaar, startdatumtijd,einddatumtijd)
		  values (@team1,@team2,@indelingnr,@poulenr,@tafelnr,null,getdate(),null)
		  PRINT 'Wedstrijd aangemaakt'

        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                 ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO


BEGIN TRANSACTION T
GO
INSERT INTO ADRES
VALUES ('1234','2','Nederlands','Straat','Arnhem'),
	   ('1235','3','Nederlands','Straat','Arnhem'),
	   ('1236','4','Nederlands','Straat','Arnhem')
GO

INSERT INTO LID (VERENIGINGCODE,LIDNR,KLASSECODE,POSTCODE,HUISNR,NTTBNUMMER,VOORNAAM,TUSSENVOEGSEL,ACHTERNAAM,GEBOORTEDATUM,TELEFOONNUMMER,GESLACHT2,MOBIELNUMMER)
VALUES ('HVB',1009,'A','1234','2',0000,'Frits','','Jansen','1980','0677889987','M',null),
	   ('HVB',1008,'A','1235','3',0001,'Freek','','Jansen','1980','0677889987','M',null),
	   ('HVB',2500,'A','1236','4',0002,'Fred','','Jansen','1980','0677889987','M',null),
	   ('HVB',2501,'A','1236','4',0003,'Freddy','','Jansen','1980','0677889987','M',null)
GO

SET IDENTITY_INSERT TOERNOOI ON
INSERT INTO TOERNOOI (TOERNOOINR,POSTCODE,HUISNR,TYPENAAM,BEGINDATUMTIJD,EINDDATUMTIJD,STARTINSCHRIJF,EINDINSCHRIJF,MINDEELNEMERS,MAXDEELNEMERS,INSCHRIJFPRIJS)
VALUES (1005,'1236','4','Ladder toernooi',DATEADD(MONTH,1,GETDATE()),DATEADD(MONTH,2,GETDATE()),DATEADD(MONTH,-1,GETDATE()),DATEADD(MONTH,1,GETDATE()),2,1000001,0)
SET IDENTITY_INSERT TOERNOOI OFF
GO

SET IDENTITY_INSERT CRITERIA ON
INSERT INTO CRITERIA (CRITERIANR,KLASSECODE,LEEFTIJDSCATEGORIE,GESLACHT)
VALUES (10077,'A','Senior','M')
SET IDENTITY_INSERT CRITERIA OFF;
GO

SET IDENTITY_INSERT KLASSEINDELING ON
INSERT INTO KLASSEINDELING (INDELINGNR,TOERNOOINR,DUBBEL)
VALUES (1077,1005,0)
SET IDENTITY_INSERT KLASSEINDELING OFF
GO

SET IDENTITY_INSERT POULE ON
INSERT INTO POULE (INDELINGNR, POULENR,NUMMERVANSETS,RONDENR)
VALUES (1077,1088,3,0)
SET IDENTITY_INSERT POULE OFF
GO

INSERT INTO CRITERIAINKLASSENINDELING
VALUES (10077,1077)
GO

SET IDENTITY_INSERT TEAM ON
INSERT INTO TEAM (	TEAMNR, LID1_VERENIGINGCODE,	LID1_LIDNR,	LID2_VERENIGINGCODE,	LID2_LIDNR,		INDELINGNR,	POULENR,	SCORE,	HEEFTBETAALD,	WACHTTIJD)
VALUES (			2055,	'HVB',					1009,		null,					null,			1077,		1088,		999,	1,				null),
	   (			2056,	'HVB',					1008,		null,					null,			1077,		1088,		998,	1,				null),
	   (			4000,	'HVB',					2500,		null,					null,			1077,		1088,		5,		1,				null),
	   (			4001,	'HVB',					2501,		null,					null,			1077,		1088,		9,		1,				null)
SET IDENTITY_INSERT TEAM OFF
GO

INSERT INTO TAFELINTOERNOOI(TAFELNR, TOERNOOINR)
    VALUES(1, 1005)

SAVE TRANSACTION T
GO

-- Tests
-- Test WG_1
PRINT 'TESTS'
GO
PRINT '----------------------------------'
PRINT 'EXPECTED: Wedstrijd aangemaakt'
exec proc_createWedstrijdLaddertoernooi 2055,2056
GO
/*
-- Test WG_2
PRINT '----------------------------------'
PRINT 'Updating wedstrijd..'
UPDATE WEDSTRIJD
SET EINDDATUMTIJD = GETDATE()
WHERE TEAMNR1 = 2055 AND TEAMNR2 = 2056
GO
select * from team
PRINT '----------------------------------'
PRINT 'EXPECTED: Deze twee teams mogen nog geen wedstrijd spelen. Er moet minimaal twee maanden wachttijd tussen zitten.'
exec proc_createWedstrijdLaddertoernooi 2055,2056

GO
*/

select * from lid
GO

/*
-- Test WG_3
PRINT '----------------------------------'
PRINT 'EXPECTED: Deze twee teams mogen nog geen wedstrijd spelen. Er moet minimaal twee maanden wachttijd tussen zitten.'
exec proc_createWedstrijdLaddertoernooi 2056,2055
GO
PRINT '----------------------------------'
GO
*/

-- Test WG_4
PRINT 'TESTS'
PRINT '----------------------------------'
PRINT 'EXPECTED: Deze teams mogen geen wedstrijd spelen omdat hun rang teveel verschilt.'
exec proc_createWedstrijdLaddertoernooi 4001,4000
GO
ROLLBACK TRANSACTION T