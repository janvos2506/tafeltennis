USE TafelTennis;
GO

-- Constraint
ALTER TABLE LID DROP CONSTRAINT CK_Lid_klasse 
GO

ALTER TABLE LID
ADD CONSTRAINT CK_Lid_klasse CHECK(NTTBNUMMER IS NOT NULL
                                   OR
                                   NTTBNUMMER IS NULL
                                   AND
                                   KLASSECODE = 'I');
GO

-- Test data
UPDATE LID
       SET KLASSECODE = 'I'
WHERE NTTBNUMMER IS NULL;
GO

-- Tests

-- Test CH_1 
BEGIN TRAN;
PRINT ''
PRINT ''
PRINT 'Expected Ok.'
INSERT INTO LID ( VERENIGINGCODE , LIDNR , KLASSECODE , POSTCODE , HUISNR , NTTBNUMMER , VOORNAAM , TUSSENVOEGSEL , ACHTERNAAM , GEBOORTEDATUM , TELEFOONNUMMER , GESLACHT2)
	    VALUES ( 'HVB' , 2000 , 'A' , 1674 , 54 , 85484 , 'Henk' , 'van' , 'henk' , GETDATE() , '026-33363' , 'M');

ROLLBACK;
GO

-- Test CH_2
BEGIN TRAN;
PRINT ''
PRINT ''
PRINT 'Expected Error: The INSERT statement conflicted with the CHECK constraint "CK_Lid_klasse". The conflict occurred in database "TafelTennis", table "dbo.LID".'
INSERT INTO LID ( VERENIGINGCODE , LIDNR , KLASSECODE , POSTCODE , HUISNR , NTTBNUMMER , VOORNAAM , TUSSENVOEGSEL , ACHTERNAAM , GEBOORTEDATUM , TELEFOONNUMMER , GESLACHT2)
VALUES ( 'HVB' , 2000 , 'A' , 1674 , 54 , NULL , 'Henk' , 'van' , 'henk' , GETDATE() , '026-33363' , 'M');
ROLLBACK;
GO

