USE TafelTennis;
GO

-- Constraint
DROP TRIGGER tr_checkOfKlasseIndelingAlBestaat
GO

CREATE TRIGGER tr_checkOfKlasseIndelingAlBestaat ON KLASSEINDELING
AFTER INSERT
AS
     BEGIN
         IF @@ROWCOUNT = 0
			RETURN;
		SET NOCOUNT ON;
		BEGIN TRY
			IF EXISTS
            (
				SELECT t.TOERNOOINR, c.CRITERIANR FROM TOERNOOI t
					INNER JOIN KLASSEINDELING k ON
						t.TOERNOOINR = k.TOERNOOINR
					INNER JOIN CRITERIAINKLASSENINDELING ck ON
						k.INDELINGNR = ck.INDELINGNR
					INNER JOIN CRITERIA c ON
						ck.CRITERIANR = c.CRITERIANR
					GROUP BY t.TOERNOOINR, c.CRITERIANR
					HAVING COUNT(c.CRITERIANR) >1
            )
                 BEGIN
                     DECLARE @errormsg VARCHAR(MAX)= 'Deze klassenindeling bestaat al.';
                     THROW 50004, @errormsg, 1;
                 END;
         END TRY
         BEGIN CATCH
             THROW;
         END CATCH;
     END;
GO

-- Reset identities
DBCC CHECKIDENT (KLASSEINDELING, RESEED, 0);
GO

-- Test Data
DECLARE @adresPostcode VARCHAR(256) = (
SELECT TOP 1 POSTCODE FROM ADRES)

DECLARE @adresHuisnr VARCHAR(256) = (
SELECT TOP 1 HUISNR FROM ADRES)

SET IDENTITY_INSERT TOERNOOI ON
INSERT INTO TOERNOOI(
TOERNOOINR, POSTCODE,			HUISNR,			TYPENAAM,				BEGINDATUMTIJD,		EINDDATUMTIJD,		STARTINSCHRIJF, EINDINSCHRIJF,	MINDEELNEMERS,	MAXDEELNEMERS,	INSCHRIJFPRIJS) VALUES(
1,			@adresPostcode,		@adresHuisnr,	'Ladder Toernooi',		getDate() +10,		getDate() +20,		getDate(),		getDate() + 2,		4,				40,				3.50)	,	(
2,			@adresPostcode,		@adresHuisnr,	'Ladder Toernooi',		getDate() +15,		getDate() +25,		getDate()+5,	getDate() + 7,		8,				60,				3.50)	,	(
3,			@adresPostcode,		@adresHuisnr,	'Ladder Toernooi',		getDate() +25,		getDate() +55,		getDate()+15,	getDate() + 20,		20,				80,				3.50)
SET IDENTITY_INSERT TOERNOOI OFF
GO

INSERT INTO KLASSEINDELING(
TOERNOOINR,		DUBBEL) VALUES (
	1,			1)		,(
	1,			1)		,(
	1,			1)		,(
	1,			1)		,(
	2,			1)		,(
	3,			1)
GO

SET IDENTITY_INSERT CRITERIA ON
INSERT INTO CRITERIA(
CRITERIANR,	KLASSECODE,		LEEFTIJDSCATEGORIE,		GESLACHT) VALUES (
	1,			'A',			'Senior',			'M'),(
	2,			'B',			'Senior',			'M'),(
	3,			'C',			'Senior',			'M'),(
	4,			'D',			'Senior',			'M'),(
	5,			'A',			'Junior',			'M'),(
	6,			'A',			'Senior',			'V'),(
	7,			'B',			'Senior',			'V')
SET IDENTITY_INSERT CRITERIA OFF
GO

INSERT INTO CRITERIAINKLASSENINDELING(
CRITERIANR, INDELINGNR) VALUES (
	1,			1)		,(
	2,			1)		,(
	3,			2)		,(
	4,			2)		,(
	5,			3)		,(
	6,			4)		,(
	7,			4)
GO

-- Tests
--Test 1
PRINT 'TEST TK_1'
PRINT ''
PRINT 'Expected ok'
INSERT INTO CRITERIAINKLASSENINDELING(
CRITERIANR,	INDELINGNR) VALUES (
	8,			5)
GO

--Test 2
PRINT 'TEST TK_2'
PRINT ''
PRINT 'Expected ok'
INSERT INTO CRITERIAINKLASSENINDELING(
CRITERIANR,	INDELINGNR) VALUES (
	1,			5)		,(
	2,			5)
GO

--TEST 2
PRINT 'TEST TK_2'
PRINT ''
PRINT 'Expected error'
INSERT INTO CRITERIAINKLASSENINDELING(
CRITERIANR,	INDELINGNR) VALUES (
	1,			5)
GO

-- Delete inserted test data
DELETE FROM CRITERIAINKLASSENINDELING
DELETE FROM KLASSEINDELING
DELETE FROM CRITERIA
DELETE FROM TOERNOOI
GO