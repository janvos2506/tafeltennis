USE TafelTennis
GO

DROP PROCEDURE proc_createWedstrijd
GO

CREATE PROCEDURE proc_createWedstrijd
@team1 INT,
@team2 INT
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
    BEGIN TRY

	DECLARE @indelingnr INT
	DECLARE @poulenr INT
	DECLARE @tafelnr INT
	DECLARE @toernooirnr INT

	SET @indelingnr = (select indelingnr from team where teamnr = @team1)
	SET @poulenr = (select poulenr from team where teamnr = @team1)
	SET @toernooirnr = (select toernooinr from klasseindeling where indelingnr = @indelingnr)
	SET @tafelnr = (select tafelnr from tafelintoernooi where toernooinr = @toernooirnr)

		  insert into wedstrijd (teamnr1, teamnr2, indelingnr, poulenr, tafelnr, winnaar, startdatumtijd,einddatumtijd)
		  values (@team1,@team2,@indelingnr,@poulenr,@tafelnr,null,null,null)
		  PRINT 'Wedstrijd aangemaakt'
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO


BEGIN TRANSACTION T
GO
INSERT INTO ADRES
VALUES ('1234','2','Nederlands','Straat','Arnhem'),
	   ('1235','3','Nederlands','Straat','Arnhem'),
	   ('1236','4','Nederlands','Straat','Arnhem')
GO

INSERT INTO VERENIGING
VALUES ('HVB','Het vrolijke batje',1)
GO

INSERT INTO KLASSE
VALUES ('A')
GO

INSERT INTO LID (VERENIGINGCODE,LIDNR,KLASSECODE,POSTCODE,HUISNR,NTTBNUMMER,VOORNAAM,TUSSENVOEGSEL,ACHTERNAAM,GEBOORTEDATUM,TELEFOONNUMMER,GESLACHT2,MOBIELNUMMER)
VALUES ('HVB',1009,'A','1234','2',0000,'Frits','','Jansen','1980','0677889987','M',null),
	   ('HVB',1008,'A','1235','3',0001,'Freek','','Jansen','1980','0677889987','M',null)
GO

INSERT INTO TOERNOOIVORM
VALUES ('Ladder toernooi',0)
GO

SET IDENTITY_INSERT TOERNOOI ON
INSERT INTO TOERNOOI (TOERNOOINR,POSTCODE,HUISNR,TYPENAAM,BEGINDATUMTIJD,EINDDATUMTIJD,STARTINSCHRIJF,EINDINSCHRIJF,MINDEELNEMERS,MAXDEELNEMERS,INSCHRIJFPRIJS)
VALUES (1005,'1236','4','Ladder toernooi',DATEADD(MONTH,1,GETDATE()),DATEADD(MONTH,2,GETDATE()),DATEADD(MONTH,-1,GETDATE()),DATEADD(MONTH,1,GETDATE()),2,2,0)
SET IDENTITY_INSERT TOERNOOI OFF
GO

SET IDENTITY_INSERT CRITERIA ON
INSERT INTO CRITERIA (CRITERIANR,KLASSECODE,LEEFTIJDSCATEGORIE,GESLACHT)
VALUES (10077,'A','Senior','M')
SET IDENTITY_INSERT CRITERIA OFF
GO

SET IDENTITY_INSERT KLASSEINDELING ON
INSERT INTO KLASSEINDELING (INDELINGNR,TOERNOOINR,DUBBEL)
VALUES (1077,1005,0)
SET IDENTITY_INSERT KLASSEINDELING OFF
GO

SET IDENTITY_INSERT POULE ON
INSERT INTO POULE (INDELINGNR, POULENR,NUMMERVANSETS,RONDENR)
VALUES (1077,1088,3,0)
SET IDENTITY_INSERT POULE OFF
GO

INSERT INTO CRITERIAINKLASSENINDELING
VALUES (10077,1077)
GO

SET IDENTITY_INSERT TEAM ON
INSERT INTO TEAM (TEAMNR, LID1_VERENIGINGCODE,LID1_LIDNR,LID2_VERENIGINGCODE,LID2_LIDNR,INDELINGNR,POULENR,SCORE,HEEFTBETAALD,WACHTTIJD)
VALUES (2055,'HVB',1009,null,null,1077,1088,999,1,null),
	   (2056,'HVB',1008,null,null,1077,1088,998,1,null)
SET IDENTITY_INSERT TEAM OFF
GO

-- Tests
-- Test WZ_1
PRINT 'TESTS'
PRINT '----------------------------------'
PRINT 'EXPECTED: Wedstrijd aangemaakt'
exec proc_createWedstrijd 2055,2056
GO
ROLLBACK TRANSACTION T
GO

