USE TafelTennis;
GO

-- Constraint
DROP PROCEDURE sp_linkTafelAanWedstrijd;
GO

CREATE PROCEDURE sp_linkTafelAanWedstrijd @tafelnr INT, @wedstrijdnr INT, @startTime DATE
AS
     SET NOCOUNT, XACT_ABORT ON;
     DECLARE @TranCounter INT;
     SET @TranCounter = @@TRANCOUNT;
     IF @TranCounter > 0
         SAVE TRANSACTION ProcedureSave;
     ELSE
     BEGIN TRANSACTION;
	 SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
    BEGIN TRY
		  -- Code here
		  IF EXISTS (
			SELECT * FROM WEDSTRIJD w 
				INNER JOIN TAFEL t 
					ON w.TAFELNR = t.TAFELNR
				WHERE w.STARTDATUMTIJD < @startTime AND (w.EINDDATUMTIJD IS NULL OR w.EINDDATUMTIJD > @startTime)
		  )BEGIN
			 DECLARE @errormsg VARCHAR(MAX)= 'Er is al een wedstrijd bezig op deze tafel.';
                THROW 50004, @errormsg, 1;
		  END
		  ELSE
		  BEGIN
			 UPDATE WEDSTRIJD
			 SET TAFELNR = @tafelnr WHERE WEDSTRIJDNR = @wedstrijdnr
		  END
        IF @TranCounter = 0
           AND XACT_STATE() = 1
            COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
	   --Error handling
        DECLARE @ErrorMessage VARCHAR(400), @ErrorSeverity INT, @ErrorState INT;
        SELECT @ErrorMessage = ERROR_MESSAGE(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE();
        RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        IF @TranCounter = 0
            BEGIN
                IF XACT_STATE() = 1
                    ROLLBACK TRANSACTION;
            END;
        ELSE
            BEGIN
                /*If this procedure was executed within an other transaction*/
                IF XACT_STATE() <> -1
                    ROLLBACK TRANSACTION ProcedureSave;
            END;
        THROW;
    END CATCH;
GO

-- Reset identities
DBCC CHECKIDENT (TOERNOOI, RESEED, 0);		   
DBCC CHECKIDENT (KLASSEINDELING, RESEED, 0);	  
DBCC CHECKIDENT (POULE, RESEED, 0);		  
DBCC CHECKIDENT (TEAM, RESEED, 0);			  
GO

-- Test data
INSERT INTO TOERNOOI(POSTCODE, HUISNR, TYPENAAM, BEGINDATUMTIJD, EINDDATUMTIJD, STARTINSCHRIJF, EINDINSCHRIJF, MINDEELNEMERS, MAXDEELNEMERS, INSCHRIJFPRIJS)
			VALUES(1821, 8, 'Ladder Toernooi', GETDATE() + 10, GETDATE() + 15, GETDATE() - 5, GETDATE() + 5, 0, 100, 0);
INSERT INTO KLASSEINDELING(TOERNOOINR, DUBBEL)
			VALUES(1, 0)
INSERT INTO POULE(INDELINGNR, NUMMERVANSETS, RONDENR)
			VALUES(1, 3, 0)
INSERT INTO TEAM(LID1_LIDNR, LID1_VERENIGINGCODE, LID2_LIDNR, LID2_VERENIGINGCODE, INDELINGNR, POULENR, SCORE, HEEFTBETAALD)
			VALUES	(1, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(2, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(3, 'HVB', NULL, NULL, 1, 1, 0, 1),
					(4, 'HVB', NULL, NULL, 1, 1, 0, 1);
GO

-- Tests
-- Test TT_2
PRINT ''
PRINT ''
PRINT 'Expected Ok.'
EXEC sp_linkTafelAanWedstrijd @team1 = 1, @team2 = 2, @tafelnr = 1, @startTime = '2000-01-01';
GO

-- Test TT_1
PRINT ''
PRINT ''
PRINT 'Expected Error.'
EXEC sp_linkTafelAanWedstrijd @team1 = 3, @team2 = 4, @tafelnr = 1, @startTime = '2000-01-02';
GO

-- Delete inserted test data
DELETE FROM WEDSTRIJD;
DELETE FROM TEAM;
DELETE FROM POULE;
DELETE FROM KLASSEINDELING;
DELETE FROM TOERNOOI;
GO