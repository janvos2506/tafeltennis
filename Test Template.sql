USE TafelTennis;
GO

-- Constraint
DROP TRIGGER/PROC  
GO

	   -- Insert Trigger/Proc here
GO

-- Reset identities
DBCC CHECKIDENT (TABLE, RESEED, 0)
GO

	   -- Insert test data

GO

-- Tests
PRINT ''
PRINT ''
PRINT 'Expected Ok/Error.'  -- Give expected error message
-- Insert test here
GO

-- Delete inserted test data

GO